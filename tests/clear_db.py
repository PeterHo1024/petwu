# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
import os
import cPickle as pickle
import requests
import django

"""
删除测试数据
"""

__author__ = 'peter'

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.settings")

django.setup()

from apps.img.models import Img
from tests.foster import FosterSitter
from django.contrib.auth.models import User
from apps.user.models import UserInfo
from apps.sitter.models import Sitter, SitterSpecialService
from django.core.files.base import ContentFile
from apps.master.models import Master

root = os.path.dirname(__file__)


def clear_db():
    User.objects.filter(username__regex='^user.').delete()
    Master.objects.filter(nickname__regex='^user.').delete()


# Start execution here!
if __name__ == '__main__':
    print("Starting Pet-Wu clear db script...")
    clear_db()
