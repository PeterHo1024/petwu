# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
import os
import cPickle as pickle
import requests
import django

"""
删除测试数据
"""

__author__ = 'peter'

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.settings")

django.setup()

from apps.img.models import Img
from tests.foster import FosterSitter
from django.contrib.auth.models import User
from apps.user.models import UserInfo
from apps.sitter.models import Sitter, SitterSpecialService
from django.core.files.base import ContentFile
from apps.master.models import Master

root = os.path.dirname(__file__)


def change_db():
    sitters = Sitter.objects.all()
    for sitter in sitters:
        longitude = sitter.longitude
        latitude = sitter.latitude
        if longitude and latitude:
            sitter.longitude = latitude
            sitter.latitude = longitude
            sitter.save()


# Start execution here!
if __name__ == '__main__':
    print("Starting Pet-Wu clear db script...")
    change_db()
