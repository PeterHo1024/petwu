# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
import os
import cPickle as pickle
import requests
import django

__author__ = 'peter'

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.settings")

django.setup()

from apps.img.models import Img
from tests.foster import FosterSitter
from django.contrib.auth.models import User
from apps.user.models import UserInfo
from apps.sitter.models import Sitter, SitterSpecialService
from django.core.files.base import ContentFile
from apps.master.models import Master

root = os.path.dirname(__file__)


def load_sitters():
    sitters = []
    f = open(root + '/sitters.txt', 'rb')
    while True:
        try:
            sitters.append(pickle.load(f))
        except EOFError, e:
            break
    f.close()
    return sitters


def populate():
    add_sitter_special_service()
    sitters = load_sitters()
    for sitter in sitters:
        user_info = add_user(sitter)
        add_sitter(user_info, sitter)


def add_user(sitter):
    username = 'user' + str(sitter.index)
    email = username + '@pet-wu.com'
    password = username
    user_info = UserInfo.create_user(username, password, email)
    return user_info


def add_sitter(user_info, foster_sitter):
    sitter = foster_sitter.sitter
    sitter.manager = user_info
    sitter.province = "四川省"
    sitter.city = "成都市"
    sitter.latitude = 30.1
    sitter.longitude = 104.1
    sitter.save()
    user_info.sitter = sitter
    user_info.save()

    # 下载头像图片
    img = Img()
    img.user = user_info
    img.img.save('head_portrait', ContentFile(requests.get(foster_sitter.head_portrait).content))
    img.type = 'sitter_head'
    img.desc = sitter.nickname + "_head_portrait"
    img.be_used = True
    img.owner_sitter = sitter
    img.save()

    sitter.head_portrait = img
    sitter.save()

    # 下载环境图片
    for env_photo in foster_sitter.imgs:
        img = Img()
        img.user = user_info
        img.img.save('env_photo', ContentFile(requests.get(env_photo).content))
        img.type = 'env_photo'
        img.desc = sitter.nickname + "_env_photo"
        img.be_used = True
        img.owner_sitter = sitter
        img.save()


def add_pet():
    pass


def add_master():
    pass


def add_sitter_special_service():
    SitterSpecialService.objects.all().delete()
    ss = SitterSpecialService(text="遛狗", img="liugou")
    ss.save()
    ss = SitterSpecialService(text="狗粮", img="gouliang")
    ss.save()
    ss = SitterSpecialService(text="接送", img="jiesong")
    ss.save()
    ss = SitterSpecialService(text="体检", img="tijian")
    ss.save()
    ss = SitterSpecialService(text="洗澡", img="xizao")
    ss.save()


# Start execution here!
if __name__ == '__main__':
    print("Starting Pet-Wu population script...")
    populate()
