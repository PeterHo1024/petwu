# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
import cPickle as pickle
import os

import django
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, WebDriverException

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.settings")

django.setup()

from apps.sitter.models import Sitter

"""
通过Selenium和PhantomJS抓取Foster的寄养师信息,方便测试功能

sudo apt-get install phantomjs
http://phantomjs.org/download.html
"""

__author__ = 'peter'


class FosterSitter():
    def __init__(self):
        self.index = 0
        self.sitter = Sitter()
        self.head_portrait = ''
        self.imgs = []


def get_foster_sitters():
    driver = webdriver.PhantomJS()

    f = open('sitters.txt', 'wb')

    for sitter_id in range(1, 100):
        try:
            print(sitter_id)
            driver.get("http://foster.goumin.com/foster/view.html?id=" + str(sitter_id))

            foster_sitter = FosterSitter()
            sitter = foster_sitter.sitter

            foster_sitter.index = sitter_id

            dian_txt1 = driver.find_element_by_id('dian_txt1').text
            if dian_txt1 == '店家介绍':
                sitter.sitter_type = '宠物店'
            else:
                sitter.sitter_type = '寄养师'

            detail_user = driver.find_element_by_id('detail_user')
            sitter.nickname = detail_user.find_element_by_class_name('name').text
            if len(sitter.nickname) == 0:
                continue
            foster_sitter.head_portrait = detail_user.find_element_by_tag_name('img').get_attribute('src')
            try:
                sitter.star = int(detail_user.find_element_by_tag_name('i').get_attribute('class')[1:])
            except e:
                sitter.star = 0

            sitter.phone = driver.find_element_by_id('tel_service').get_attribute('href')[4:]

            sitter.desc = driver.find_element_by_id('master_info').text

            family_infos = driver.find_element_by_id('family_info').find_elements_by_tag_name('dd')
            try:
                sitter.space = int(family_infos[0].text[4:-1])
            except ValueError, e:
                sitter.space = 0
            sitter.occupation = family_infos[1].text[4:]
            try:
                sitter.experience = int(family_infos[2].text[4:-1])
            except ValueError, e:
                sitter.experience = 0
            sitter.street = family_infos[3].text[4:]

            cate_name = driver.find_element_by_id('cate_name')
            lis = cate_name.find_elements_by_tag_name('li')
            for li in lis:
                price = li.text.replace('￥\n/天', '').split()
                if price[0] == '幼犬':
                    sitter.pup_price = int(price[1])
                    sitter.pup_price_enable = True
                elif price[0] == '小型犬':
                    sitter.small_dog_price = int(price[1])
                    sitter.small_dog_price_enable = True
                elif price[0] == '中型犬':
                    sitter.medium_dog_price = int(price[1])
                    sitter.medium_dog_price_enable = True
                elif price[0] == '大型犬':
                    sitter.big_dog_price = int(price[1])
                    sitter.big_dog_price_enable = True
                elif price[0] == '猫咪':
                    sitter.cat_price = int(price[1])
                    sitter.cat_price_enable = True
                elif price[0] == '其他':
                    sitter.others_price = int(price[1])
                    sitter.others_price_enable = True

            # feature_infos = driver.find_element_by_id('feature_info').find_elements_by_tag_name('dd')
            # for info in feature_infos:
            #     sitter.special_services.append(info.text)

            imgs = driver.find_elements_by_css_selector(".rsImg.rsMainSlideImage")
            for img in imgs:
                foster_sitter.imgs.append(img.get_attribute('src'))

            pickle.dump(foster_sitter, f)
        except NoSuchElementException, e:
            pass
        except WebDriverException, e:
            pass
            # except StandardError, e:
            #     print(e.message)

    driver.quit()

    f.close()


if __name__ == '__main__':
    get_foster_sitters()
