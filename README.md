## 项目简介
* 针对宠物寄养服务的微信服务号开发

## 技术选型
### 前端
* HTML5
* JavaScript(jQuery)
* CSS(BootStrap/jQuery Mobile)

### 后端
* Python (Django)

## 主要功能
### 寄养者
* 寄养师列表
    * 可按照距离,状态,等级,价格等方式排序,筛选
* 查看宠物
    * 被寄养宠物当前状态
* 联系寄养师
* 设置宠物信息
    * 包括品种,年龄,体重,疫苗情况等
* 预约寄养师
    * 下单,支付,评价

### 寄养师
* 申请成为寄养师
* 设置状态,地址,价格等信息
* 接受预约
* 发布宠物状态

### 管理后台
* 管理所有寄养师,寄养者,寄养宠物等信息
* 微信支付等

## 设计
### 微信
* 我要寄养
* 招寄养师/成为寄养师
* 我的
    * 个人主页
    * 爱宠状态
    * 互动社区
    * 联系客服
    * 相关规范

### 网页




## 参考产品
### 服务号
* [宠托邦](http://foster.goumin.com/foster/index.html)
* [宠物帮](http://m.pet-union.com/petkeeper/index)
* [宠乐窝](http://home.joypet.com.cn/)

### App
* [小狗在家](http://new.viptail.com/)


