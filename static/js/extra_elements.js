/*
 * 此插件定义了一系列的表单控件
 *
 * button-checkbox
 * 长得像按钮一样的checkbox
 * 需要在class中定义button-checkbox
 * <label class="button-checkbox">
 *     <input type="checkbox" name="input_name" value="input_value" checked>
 * </label>
 *
 * radio-button
 * 长得像按钮一样的radio
 * 需要在class中定义button-radio
 * <label class="button-radio">
 *     <input type="radio" name="input_name" value="input_value" checked>
 * </label>
 */

// 初始化所有class为button-checkbox的label,使其显示为一个button
function button_checkbox() {

    // 获得所有满足条件的label
    var checkboxes = $("label.button-checkbox");
    // 初始化checkbox
    checkboxes.each(function () {
        $(this).addClass('btn');
        var checkbox = $(this).children("input[type='checkbox']");
        if (checkbox.prop('checked')) {
            $(this).addClass('btn-success');
        } else {
            $(this).addClass('btn-default');
        }
        checkbox.hide();
    });
    // 定义这类按钮的click事件
    checkboxes.click(function () {
        var checkbox = $(this).children("input[type='checkbox']");
        if (checkbox.prop('checked')) {
            $(this).removeClass("btn-default").addClass("btn-success");
        } else {
            $(this).removeClass("btn-success").addClass("btn-default");
        }
    });
}


function button_radio() {

    // 获得所有满足条件的label
    var radios = $("label.button-radio");
    // 初始化radio
    radios.each(function () {
        $(this).addClass('btn');
        var radio = $(this).children("input[type='radio']");
        if (radio.prop('checked')) {
            $(this).addClass('btn-success');
        } else {
            $(this).addClass('btn-default');
        }
        radio.hide();
    });
    // 定义这类按钮的click事件
    radios.click(function () {
        var radio = $(this).children("input[type='radio']");
        if (radio.prop('checked')) {
            // 本身已选中,不管
        } else {
            // 本身未选中,选中自己,并且将所有其他同名的按钮切换为未选中
            radios.filter("[name='" + $(this).attr('name') + "']").removeClass("btn-success").addClass("btn-default");
            $(this).removeClass("btn-default").addClass("btn-success");
        }
    });
}
