* docs 文档

* petwu  项目根目录

* petwu/config 配置根目录

* petwu/config/settings 设置根目录

* petwu/templates 公用模板

* petwu/static 公用静态文件

* petwu/common 公用代码根目录

* petwu/users 用户模块根目录

* petwu/master 寄养者模块根目录

* petwu/sitter 寄养师模块根目录

* petwu/pet 宠物模块根目录

* petwu/order 订单模块根目录

* requirements 依赖配置目录

