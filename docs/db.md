## 数据库设计

* users
用户信息,主要用于登陆
一个用户可以同时是寄养者和寄养师
多个用户可以对应同一个寄养师,但只有一个是管理员

| id | username | email | phone | password | status | master_id | sitter_id | sitter_status |
|-|
| |用户名|邮箱|电话|密码|状态|寄养者id one-to-one|寄养师id one-to-many|寄养师状态|


* pets
宠物信息
与masters表构成多对一关系

| id | master_id | species | breed | age | weight |
|-|
| |主人id|物种(狗,猫,其他)|品种(拉布拉多,哈士奇)|年龄|体重|



* masters
寄养者信息
一个寄养者可以有多个宠物
通过查询pets表中的master_id字段来获得寄养者拥有的宠物

| id | user_id | nickname | phone | status | head_portrait |
|-|
| |用户id|昵称|电话|状态|头像|


* sitters
寄养师信息
可能常用信息和详细信息要分表存放

| id | manager | nickname | phone | status | sitter_type | desc |
|-|
| |管理员|昵称|电话|状态|类型(宠物店,专业寄养点,家庭寄养师)|个人描述|

| realname | sex | email | id_number | occupation |
|-|
|真实姓名|性别|邮箱|身份证号|职业|

| province | city | street | house_number | space |
|-|
|省份|城市|街道/小区|楼,单元,门牌|房间面积|

| experience | pup_price | small_dog_price | medium_dog_price | big_dog_price | cat_price | others_price |
|-|
|饲养经验| 幼犬价格 | 小型犬价格 | 中型犬价格 | 大型犬价格 | 猫咪价格 | 其他价格 |

| special_service | head_portrait | star |
|-|
|特色服务|头像|星级|


* sitters_pictures
保存寄养师的图片数据

|id|sitter_id|picture|desc|
|-|
| |寄养师id|图片数据|图片描述|

* managers
寄养师中


* orders
订单信息
与masters表构成多对一关系
与sitters表构成多对一关系

| master_id | sitter_id|
|-|-|


* order_pet
订单相关宠物的中间表
由Django的ORM自动维护
一个订单可以有多个宠物
可以通过订单id获得对应的宠物id