# -*- coding:utf-8 -*-
import os

# 设置配置文件路径
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.production")

from django.core.wsgi import get_wsgi_application
from bae.core.wsgi import WSGIApplication

application = WSGIApplication(get_wsgi_application())
