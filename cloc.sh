#!/usr/bin/env bash

# sudo apt-get install cloc
cloc . \
--exclude-list-file=./cloc/.clocignore \
--exclude-dir=$(tr '\n' ',' < ./cloc/.clocignoredirs) \
--counted=./cloc/counted.txt \
--ignored=./cloc/ignored.txt \
| tee cloc/result.txt
