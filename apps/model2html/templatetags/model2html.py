# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django import template
from math import sqrt

__author__ = 'peter'

register = template.Library()

from .jqm import *
from .bootstrap import *


# 用很粗略的方式来通过经纬度计算距离
# TODO 以后改得更精确些
@register.simple_tag
def get_distance(longitude, latitude, longitude2, latitude2):
    distance = sqrt(
        ((float(longitude) - float(longitude2)) * 95419) ** 2 +
        ((float(latitude) - float(latitude2)) * 111319) ** 2)
    if distance > 1000:
        return '%.1f' % (distance / 1000) + 'KM'
    else:
        return '%.0f' % distance + 'M'
