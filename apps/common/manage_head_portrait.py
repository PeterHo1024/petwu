# coding=utf-8
"""头像相关的管理模块"""
from __future__ import print_function, unicode_literals, absolute_import

__author__ = 'peter'


# 当对象被删除时,删除其对应的头像
def on_delete(sender, instance, **kwargs):
    if instance.head_portrait:
        instance.head_portrait.delete()


# 当对象被改变时,置对象头像的使用标志位,如果头像被改变,则删除老头像
def on_save(sender, instance, **kwargs):
    # 获取老头像
    if instance.pk:
        old_img = sender.objects.get(pk=instance.pk).head_portrait
    else:
        old_img = None
    # 获取新头像
    new_img = instance.head_portrait
    # 设置新头像的使用标志位
    if new_img and new_img.be_used is False:
        new_img.be_used = True
        new_img.save()
    # 如果头像改变,则删除老头像
    if old_img and old_img != new_img:
        old_img.delete()
