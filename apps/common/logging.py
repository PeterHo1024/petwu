# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

import logging

import time
from bae_log import handlers

from config.settings import settings

__author__ = 'peter'

logger = None


def get_logger():
    global logger
    if not logger:
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        handler = handlers.BaeLogHandler(ak=settings.bae_ak, sk=settings.bae_sk, bufcount=256)
        logger.addHandler(handler)
        logger.debug('Logger is running!')
    return logger


def writing_logging(log):
    with open("./static/log.log", "a") as f:
        f.write(time.strftime('%Y-%m-%d %X', time.localtime(time.time())) + ": " + log + "\n")


def writing_file(text):
    with open("./static/log.txt", "w") as f:
        f.write(text)
