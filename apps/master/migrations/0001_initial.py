# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('img', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Master',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nickname', models.CharField(max_length=50, verbose_name='\u6635\u79f0')),
                ('sex', models.CharField(default='\u7537', max_length=10, verbose_name='\u6027\u522b', choices=[('\u7537', '\u7537'), ('\u5973', '\u5973')])),
                ('phone', models.CharField(max_length=20, verbose_name='\u8054\u7cfb\u7535\u8bdd', blank=True)),
                ('status', models.CharField(default='\u6b63\u5e38', max_length=20, verbose_name='\u72b6\u6001', choices=[('\u6b63\u5e38', '\u6b63\u5e38')])),
                ('head_portrait', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to='img.Img', verbose_name='\u5934\u50cf')),
            ],
        ),
    ]
