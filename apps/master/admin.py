# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.contrib import admin
from .models import Master

__author__ = 'peter'

admin.site.register(Master)
