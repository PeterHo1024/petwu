# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.forms import ModelForm, RadioSelect, Select, TextInput, Textarea
from django.utils.translation import ugettext_lazy as _
from .models import Master

__author__ = 'peter'


class MasterFormBaseMeta:
    def __init__(self):
        pass

    model = Master
    widgets = {
        'sex': RadioSelect(choices=Master.sex_choices),
    }
    labels = {
    }
    help_texts = {
    }
    error_messages = [
    ]


class MasterInfoForm(ModelForm):
    class Meta(MasterFormBaseMeta):
        fields = (
            'nickname',
            'sex',
            'phone',
            'head_portrait',
        )
