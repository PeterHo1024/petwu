# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.conf.urls import url

from . import views

__author__ = 'peter'

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^setting/$', views.setting, name='setting'),
]
