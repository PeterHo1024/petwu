# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.db import models
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from apps.common import manage_head_portrait

__author__ = 'peter'


@python_2_unicode_compatible
class Master(models.Model):
    sex_choices = (
        ("男", "男"),
        ("女", "女"),
    )
    status_choices = (
        ("正常", "正常"),
    )
    # 昵称
    nickname = models.CharField("昵称", max_length=50)
    # 性别
    sex = models.CharField("性别", choices=sex_choices, max_length=10, default="男")
    # 联系电话
    phone = models.CharField("联系电话", max_length=20, blank=True)
    # 状态
    status = models.CharField("状态", choices=status_choices, max_length=20, default="正常")
    # 头像
    head_portrait = models.OneToOneField("img.Img", verbose_name="头像", null=True, blank=True, on_delete=models.SET_NULL)

    def _get_head_portrait_url(self):
        if self.head_portrait:
            return self.head_portrait.img.url
        elif self.sex == '男':
            return static("images/head_portrait/male.png")
        else:
            return static("images/head_portrait/female.png")

    # 显示用的头像路径,包含默认值
    head_portrait_url = property(_get_head_portrait_url)

    def __str__(self):
        return self.nickname


# 当Master被保存时判断头像的变化情况
@receiver(pre_save, sender=Master)
def on_save(sender, instance, **kwargs):
    manage_head_portrait.on_save(sender, instance, **kwargs)
