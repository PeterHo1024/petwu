# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.contrib.auth.decorators import login_required

from django.shortcuts import render, redirect
from django.template.context_processors import csrf
from .forms import MasterInfoForm

__author__ = 'peter'


@login_required
def home(request):
    master = request.user.userinfo.master
    ctx = {
        'master': master,
    }
    return render(request, 'master/home.html', ctx)


@login_required
def setting(request):
    master = request.user.userinfo.master
    if request.POST:
        form = MasterInfoForm(request.POST, instance=master)
        if form.is_valid():
            form.save()
            return redirect("master:home")

    master_form = MasterInfoForm(instance=master)
    ctx = {
        'master': master,
        'master_form': master_form,
    }
    ctx.update(csrf(request))
    return render(request, 'master/setting.html', ctx)
