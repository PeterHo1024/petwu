# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.context_processors import csrf
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView
from django.contrib.staticfiles.templatetags.staticfiles import static
from .form import PetForm
from .models import Pet

__author__ = 'peter'


# 添加宠物
@login_required
def add(request):
    default_redirect_url = "pet:list"
    if request.POST:
        form = PetForm(request.POST)
        if form.is_valid():
            new_pet = form.save(commit=False)
            new_pet.master = request.user.userinfo
            new_pet.save()
            next_url = request.POST.get("next_url", default_redirect_url)
            if len(next_url) == 0:
                next_url = default_redirect_url
            return redirect(next_url)

    ctx = {
        'form': PetForm(),
        'head_portrait_url': static("images/head_portrait/pet_dog.png"),
        'next_url': request.GET.get("next", default_redirect_url)
    }
    ctx.update(csrf(request))
    return render(request, "pet/add.html", ctx)


# 显示宠物详情并修改
@login_required
def detail(request, pk):
    pet = Pet.objects.get(pk=pk)
    if request.POST:
        form = PetForm(request.POST, instance=pet)
        if form.is_valid():
            new_pet = form.save(commit=False)
            new_pet.master = request.user.userinfo
            new_pet.save()
            return redirect("pet:list")

    ctx = {
        'form': PetForm(instance=pet),
        'pet': pet,
    }
    ctx.update(csrf(request))
    return render(request, "pet/detail.html", ctx)


# 显示宠物列表
class PetListView(ListView):
    model = Pet
    template_name = 'pet/list.html'
    context_object_name = 'my_pet_list'

    def get_queryset(self):
        return Pet.objects.filter(master=self.request.user.userinfo)

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(PetListView, self).dispatch(*args, **kwargs)


# 删除宠物
@login_required
def remove(request, pk):
    pet = Pet.objects.get(pk=pk)
    # 判断是否是本人
    if pet.master == request.user.userinfo:
        pet.delete()
        return HttpResponse("Success")
    return HttpResponse("None")
