# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.db import models
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.db.models.signals import pre_delete, pre_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from apps.common import manage_head_portrait

__author__ = 'peter'

pet_type_choices = (
    ("幼犬", "幼 0-7月"),
    ("小型犬", "小 0-7kg"),
    ("中型犬", "中 8-18kg"),
    ("大型犬", "大 19-45kg"),
    ("猫咪", "猫"),
    ("其他", "其他"),
)


class PetTypeInfo(object):
    type_id = 0
    type_name = 'default_name'
    type_desc = 'default_desc'

    def set_info(self, type_id, name, desc):
        self.type_id = type_id
        self.type_name = name
        self.type_desc = desc


pet_type_info_list = []
for i, pet_type in enumerate(pet_type_choices):
    info = PetTypeInfo()
    info.set_info(i, pet_type[0], pet_type[1])
    pet_type_info_list.append(info)


@python_2_unicode_compatible
class Pet(models.Model):
    sex_choices = (
        ("公", "公"),
        ("母", "母"),
    )
    # 主人
    master = models.ForeignKey('user.UserInfo')
    # 昵称
    nickname = models.CharField("昵称", max_length=50)
    # 类型
    type = models.CharField("类型", choices=pet_type_choices, default="幼犬", max_length=20)
    # 品种(拉布拉多,哈士奇)
    breed = models.CharField("品种", max_length=50)
    # 性别
    sex = models.CharField("性别", choices=sex_choices, default="公", max_length=10)
    # 年龄
    year = models.IntegerField("年龄", default=1)
    month = models.IntegerField("月份", default=1)
    # 头像
    head_portrait = models.OneToOneField("img.Img", verbose_name="头像", null=True, blank=True, on_delete=models.SET_NULL)

    def _get_head_portrait_url(self):
        if self.head_portrait:
            head_portrait_url = self.head_portrait.img.url
        elif self.type == "猫咪":
            head_portrait_url = static("images/head_portrait/pet_cat.png")
        elif self.type == "其他":
            head_portrait_url = static("images/head_portrait/pet_other.png")
        else:
            head_portrait_url = static("images/head_portrait/pet_dog.png")
        return head_portrait_url

    # 显示用的头像路径,包含默认值
    head_portrait_url = property(_get_head_portrait_url)

    def __str__(self):
        return self.nickname


# 当Pet被删除时同时删除其对应的头像
@receiver(pre_delete, sender=Pet)
def on_delete(sender, instance, **kwargs):
    manage_head_portrait.on_delete(sender, instance, **kwargs)


# 当Pet被保存时判断头像的变化情况
@receiver(pre_save, sender=Pet)
def on_save(sender, instance, **kwargs):
    manage_head_portrait.on_save(sender, instance, **kwargs)
