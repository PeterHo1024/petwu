# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.forms import ModelForm, Textarea
from django.utils.translation import ugettext_lazy as _
from .models import Pet

__author__ = 'peter'


class PetForm(ModelForm):
    class Meta:
        model = Pet
        exclude = [
            'master',
        ]
