# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('img', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nickname', models.CharField(max_length=50, verbose_name='\u6635\u79f0')),
                ('type', models.CharField(default='\u5e7c\u72ac', max_length=20, verbose_name='\u7c7b\u578b', choices=[('\u5e7c\u72ac', '\u5e7c 0-7\u6708'), ('\u5c0f\u578b\u72ac', '\u5c0f 0-7kg'), ('\u4e2d\u578b\u72ac', '\u4e2d 8-18kg'), ('\u5927\u578b\u72ac', '\u5927 19-45kg'), ('\u732b\u54aa', '\u732b'), ('\u5176\u4ed6', '\u5176\u4ed6')])),
                ('breed', models.CharField(max_length=50, verbose_name='\u54c1\u79cd')),
                ('sex', models.CharField(default='\u516c', max_length=10, verbose_name='\u6027\u522b', choices=[('\u516c', '\u516c'), ('\u6bcd', '\u6bcd')])),
                ('year', models.IntegerField(default=1, verbose_name='\u5e74\u9f84')),
                ('month', models.IntegerField(default=1, verbose_name='\u6708\u4efd')),
                ('head_portrait', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to='img.Img', verbose_name='\u5934\u50cf')),
            ],
        ),
    ]
