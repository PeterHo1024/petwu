# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.conf.urls import url

from . import views

__author__ = 'peter'


urlpatterns = [
    url(r'^$', views.PetListView.as_view(), name='list'),
    url(r'^add/$', views.add, name='add'),
    url(r'^del/(?P<pk>[0-9]+)/$', views.remove, name='del'),
    url(r'^(?P<pk>[0-9]+)/$', views.detail, name='detail'),
]
