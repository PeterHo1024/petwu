# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.conf.urls import url
from . import views

__author__ = 'peter'


urlpatterns = [
    url(r'^add/(?P<sitter_id>[0-9]+)/$', views.add, name='add'),
    url(r'^cancel/(?P<order_id>[0-9]+)/$', views.cancel, name='cancel'),
    url(r'^confirm/(?P<order_id>[0-9]+)/$', views.confirm, name='confirm'),
    url(r'^accept/(?P<order_id>[0-9]+)/$', views.accept, name='accept'),
    url(r'^decline/(?P<order_id>[0-9]+)/$', views.decline, name='decline'),
    url(r'^master_remove_order/(?P<order_id>[0-9]+)/$', views.master_remove_order, name='master_remove_order'),
    url(r'^sitter_remove_order/(?P<order_id>[0-9]+)/$', views.sitter_remove_order, name='sitter_remove_order'),
    url(r'^master_order_list/(?P<order_status>[a-z]+)/$', views.master_order_list, name='master_order_list'),
    url(r'^sitter_order_list/(?P<order_status>[a-z]+)/$', views.sitter_order_list, name='sitter_order_list'),
]
