# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('master', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pets', models.CharField(default='', max_length=50, verbose_name='\u5bc4\u517b\u5ba0\u7269')),
                ('status', models.CharField(default='ordered', max_length=20, verbose_name='\u8ba2\u5355\u72b6\u6001', choices=[('ordered', '\u9884\u7ea6\u5bc4\u517b'), ('accepted', '\u63a5\u53d7\u9884\u7ea6'), ('confirmed', '\u786e\u8ba4\u5bc4\u517b'), ('sitting', '\u5bc4\u517b\u4e2d'), ('completed', '\u5bc4\u517b\u5b8c\u6210'), ('canceled', '\u5df2\u53d6\u6d88'), ('declined', '\u5df2\u62d2\u7edd'), ('dated', '\u5df2\u8fc7\u671f')])),
                ('create_date', models.DateTimeField(auto_now_add=True, verbose_name='\u521b\u5efa\u65f6\u95f4')),
                ('begin_date', models.DateField(verbose_name='\u8d77\u59cb\u65f6\u95f4')),
                ('end_date', models.DateField(verbose_name='\u7ed3\u675f\u65f6\u95f4')),
                ('message', models.TextField(max_length=200, verbose_name='\u7559\u8a00', blank=True)),
                ('master_removed', models.BooleanField(default=False, verbose_name='\u4e3b\u4eba\u5df2\u5220\u9664\u8ba2\u5355')),
                ('sitter_removed', models.BooleanField(default=False, verbose_name='\u5bc4\u517b\u5e08\u5df2\u5220\u9664\u8ba2\u5355')),
                ('master', models.ForeignKey(to='master.Master')),
            ],
        ),
    ]
