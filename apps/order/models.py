# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from datetime import date as pydate
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

__author__ = 'peter'


@python_2_unicode_compatible
class Order(models.Model):
    status_choices = (
        ("ordered", "预约寄养"),
        ("accepted", "接受预约"),
        ("confirmed", "确认寄养"),
        ("sitting", "寄养中"),
        ("completed", "寄养完成"),
        ("canceled", "已取消"),
        ("declined", "已拒绝"),
        ("dated", "已过期"),
    )

    # 主人
    master = models.ForeignKey('master.Master')
    # 寄养师
    sitter = models.ForeignKey('sitter.Sitter')
    # 宠物 寄养宠物id字符串 用','间隔
    pets = models.CharField('寄养宠物', max_length=50, default='')
    # 状态
    status = models.CharField("订单状态", choices=status_choices, max_length=20, default="ordered")
    # 创建时间
    create_date = models.DateTimeField("创建时间", auto_now_add=True)
    # 起始时间
    begin_date = models.DateField("起始时间")
    # 结束时间
    end_date = models.DateField("结束时间")
    # 留言
    message = models.TextField("留言", max_length=200, blank=True)
    # 主人删除标志位
    master_removed = models.BooleanField("主人已删除订单", default=False)
    # 寄养师删除标志位
    sitter_removed = models.BooleanField("寄养师已删除订单", default=False)

    # 获得判断过日期的订单状态
    def _get_order_status(self):
        if self.status == 'ordered':
            if pydate.today() >= self.begin_date:
                self.status = 'dated'
                self.save()
        elif self.status == 'accepted':
            if pydate.today() >= self.begin_date:
                self.status = 'dated'
                self.save()
        elif self.status == 'confirmed':
            if self.begin_date <= pydate.today() <= self.end_date:
                self.status = 'sitting'
                self.save()
            elif pydate.today() > self.end_date:
                self.status = 'completed'
                self.save()
        elif self.status == 'sitting':
            if pydate.today() > self.end_date:
                self.status = 'completed'
                self.save()
        return self.status

    # 通过订单状态获得用于显示的订单状态文本
    def _get_order_status_text(self):
        for status in self.status_choices:
            if status[0] == self.order_status:
                return status[1]
        return '未知状态'

    order_status = property(_get_order_status)

    order_status_text = property(_get_order_status_text)

    def __str__(self):
        return self.master.nickname + "-" + self.sitter.nickname
