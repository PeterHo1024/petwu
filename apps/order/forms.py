# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.forms import ModelForm, RadioSelect, Select, TextInput, Textarea, ModelMultipleChoiceField, BooleanField
from django.utils.translation import ugettext_lazy as _
from apps.pet.models import Pet
from .models import Order

__author__ = 'peter'


class OrderFormBaseMeta(object):
    model = Order
    widgets = {
        'message': Textarea(attrs={'placeholder': '选填,限200个字符以内'}),
    }
    labels = {
    }


class OrderAddForm(ModelForm):
    class Meta(OrderFormBaseMeta):
        fields = (
            'pets',
            'begin_date',
            'end_date',
            'message'
        )

    def save(self, commit=True):
        if self.instance.master_removed is None:
            self.instance.master_removed = False
        if self.instance.sitter_removed is None:
            self.instance.sitter_removed = False
        return super(OrderAddForm, self).save(commit)
