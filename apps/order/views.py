# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.template.context_processors import csrf
from django.db.models import Q

from apps.sitter.models import Sitter
from .forms import OrderAddForm
from .models import Order

__author__ = 'peter'


@login_required
def home(request):
    ctx = {}
    return render(request, 'master/home.html', ctx)


# 用户下订单
# 成功后订单状态为预约寄养
@login_required
def add(request, sitter_id):
    userinfo = request.user.userinfo
    if request.POST:
        form = OrderAddForm(request.POST)
        if form.is_valid():
            new_order = form.save(commit=False)
            new_order.master = userinfo.master
            new_order.sitter_id = int(sitter_id)
            pets = request.POST.getlist("pets")
            new_order.pets = ",".join(pets)
            new_order.save()
            # 更新主人电话号码
            userinfo.master.phone = request.POST["phone"]
            userinfo.master.save()
            return redirect("master:home")

    ctx = {
        'form': OrderAddForm(),
        'user_info': userinfo,
        'master': userinfo.master,
        'sitter': Sitter.objects.get(pk=int(sitter_id)),
        'redirect_url_after_add_pet': reverse("order:add", kwargs={'sitter_id': sitter_id}),
    }
    ctx.update(csrf(request))
    return render(request, 'order/add.html', ctx)


# 如果订单状态为预约寄养,则寄养师可以接受预约
# 成功后订单状态为接受预约
@login_required
def accept(request, order_id):
    order = Order.objects.get(pk=order_id)
    # 判断是否是本人
    if order.sitter == request.user.userinfo.sitter:
        if order.order_status == "ordered":
            order.status = "accepted"
            order.save()
    order_status = request.GET.get('next', 'all')
    return redirect('order:sitter_order_list', order_status=order_status)


# 如果订单状态为预约寄养,则寄养师可以拒绝预约
# 成功后订单状态为已预约
@login_required
def decline(request, order_id):
    order = Order.objects.get(pk=order_id)
    # 判断是否是本人
    if order.sitter == request.user.userinfo.sitter:
        if order.order_status == "ordered":
            order.status = "declined"
            order.save()
    order_status = request.GET.get('next', 'all')
    return redirect('order:sitter_order_list', order_status=order_status)


# 如果订单状态为接受预约,则主人可以确认寄养
# 成功后订单状态为确认寄养
@login_required
def confirm(request, order_id):
    order = Order.objects.get(pk=order_id)
    # 判断是否是本人
    if order.master == request.user.userinfo.master:
        if order.order_status == "accepted":
            order.status = "confirmed"
            order.save()
    order_status = request.GET.get('next', 'all')
    return redirect('order:master_order_list', order_status=order_status)


# 如果订单状态为预约寄养或接受预约,则主人可以取消预约
# 成功后订单状态为已取消
@login_required
def cancel(request, order_id):
    order = Order.objects.get(pk=order_id)
    # 判断是否是本人
    if order.master == request.user.userinfo.master:
        if order.order_status == "ordered" or order.order_status == "accepted":
            order.status = "canceled"
            order.save()
    order_status = request.GET.get('next', 'all')
    return redirect('order:master_order_list', order_status=order_status)


@login_required
def master_remove_order(request, order_id):
    order = Order.objects.get(pk=order_id)
    # 判断是否是本人
    if order.master == request.user.userinfo.master:
        order.master_removed = True
        order.save()
    order_status = request.GET.get('next', 'all')
    return redirect('order:master_order_list', order_status=order_status)


@login_required
def sitter_remove_order(request, order_id):
    order = Order.objects.get(pk=order_id)
    # 判断是否是本人
    if order.sitter == request.user.userinfo.sitter:
        order.sitter_removed = True
        order.save()
    order_status = request.GET.get('next', 'all')
    return redirect('order:sitter_order_list', order_status=order_status)


@login_required
def master_order_list(request, order_status):
    userinfo = request.user.userinfo
    orders = Order.objects.filter(master=userinfo.master).filter(master_removed=False)
    if order_status != 'all':
        if order_status == 'others':
            orders = orders.filter(Q(status='canceled') | Q(status='declined') | Q(status='dated'))
        else:
            orders = orders.filter(status=order_status)
    ctx = {
        'orders': orders,
        'next': order_status,
    }
    return render(request, 'order/master_order_list.html', ctx)


@login_required
def sitter_order_list(request, order_status):
    userinfo = request.user.userinfo
    orders = Order.objects.filter(sitter=userinfo.sitter).filter(sitter_removed=False)
    if order_status != 'all':
        if order_status == 'others':
            orders = orders.filter(Q(status='canceled') | Q(status='declined') | Q(status='dated'))
        else:
            orders = orders.filter(status=order_status)
    ctx = {
        'orders': orders,
        'next': order_status,
    }
    return render(request, 'order/sitter_order_list.html', ctx)
