# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.template.context_processors import csrf
from django.contrib.staticfiles.templatetags.staticfiles import static

from apps.img.models import Img
from apps.pet.models import Pet
from apps.weixin.utils import get_get_location_ctx
from .models import Sitter, SitterSpecialService
from .forms import SitterForm, SitterInfoForm, SitterAddressForm, SitterPetForm, SitterMyPetForm, SitterFilterForm

__author__ = 'peter'


# 显示寄养师详情
@login_required
def detail(request, pk):
    sitter = Sitter.get(pk)
    env_photos = Img.get_sitter_env_photos(sitter)
    ctx = {
        'sitter': sitter,
        'env_photos': env_photos,
    }
    return render(request, 'sitter/detail.html', ctx)


# 显示寄养师列表
@login_required
def list_sitters(request):
    ctx = get_get_location_ctx(request)
    ctx['form'] = SitterFilterForm

    return render(request, 'sitter/list.html', ctx)


# 通过ajax获得寄养师列表分页
@login_required
def list_sitters_page(request, page):
    search_value = request.GET.get('searchValue', '')
    queryset = Sitter.get_all()
    # 搜索用户昵称或地址
    queryset = Sitter.search_nickname_or_address(queryset, search_value)
    # 过滤
    filter_form = SitterFilterForm(request.GET)
    if filter_form.is_valid():
        beg_date = filter_form.cleaned_data['beg_date']
        end_date = filter_form.cleaned_data['end_date']
        sitter_type = filter_form.cleaned_data['sitter_type']
        pet_type = filter_form.cleaned_data['pet_type']
        # 过滤寄养日期
        queryset = Sitter.filter_date(queryset, beg_date, end_date)
        # 过滤寄养师类型
        queryset = Sitter.filter_sitter_type(queryset, sitter_type)
        # 过滤宠物类型
        queryset = Sitter.filter_pet_type(queryset, pet_type)
    # 按距离排序
    longitude = request.GET.get('longitude', None)
    latitude = request.GET.get('latitude', None)
    queryset = Sitter.sort_by_distance(queryset, longitude, latitude)

    # 分页并返回页面
    paginator = Paginator(queryset, 10)
    try:
        sitters = paginator.page(page)
    except PageNotAnInteger:
        sitters = None
    except EmptyPage:
        sitters = None
    return render(request, 'sitter/list_page.html',
                  {
                      'longitude': longitude,
                      'latitude': latitude,
                      'sitters': sitters
                  })


# 申请寄养师
@login_required
def add(request):
    user_info = request.user.userinfo
    if request.POST:
        form = SitterForm(request.POST)
        if form.is_valid():
            # 保存寄养师信息
            special_services = request.POST.getlist("special_services")
            new_sitter = form.save(user_info=user_info, special_services=special_services)
            # 修改寄养师对应用户的信息
            user_info.add_new_setter(new_sitter)
            # 保存环境照片
            env_photo_ids = request.POST.getlist("env_photo")
            for env_photo_id in env_photo_ids:
                env_photo = Img.objects.get(pk=env_photo_id)
                if env_photo:
                    env_photo.owner_sitter = new_sitter
                    env_photo.be_used = True
                    env_photo.save()

            return redirect("sitter:manage")

    # 如果已经是寄养师,就直接到寄养师后台页面
    if user_info.sitter:
        return redirect("sitter:manage")

    # 到申请寄养师页面
    form = SitterForm()
    ss = SitterSpecialService.objects.filter(enable=True)
    ctx = get_get_location_ctx(request)
    ctx['form'] = form
    ctx['ss'] = ss
    ctx['head_portrait_url'] = static("images/head_portrait/male.png")
    ctx.update(csrf(request))
    return render(request, 'sitter/add.html', ctx)


# 寄养师后台页面
@login_required
def manage(request):
    user_info = request.user.userinfo
    sitter = user_info.sitter
    if sitter is None:
        return redirect("sitter:add")
    ctx = {
        'user_info': user_info,
        'sitter': sitter,
    }
    return render(request, 'sitter/manage.html', ctx)


@login_required
def manage_setting(request):
    user_info = request.user.userinfo
    sitter = user_info.sitter
    try:
        my_ss = [int(n) for n in sitter.special_service.split(',')]
    except ValueError:
        my_ss = None
    ctx = {
        'sitter': sitter,
        'info_form': SitterInfoForm(instance=sitter),
        'ss': SitterSpecialService.objects.filter(enable=True),
        'my_ss': my_ss,
        'address_form': SitterAddressForm(instance=sitter),
        'pet_form': SitterPetForm(instance=sitter),
        'my_pet_list': Pet.objects.filter(master=user_info),
        'redirect_url_after_add_pet': reverse("sitter:manage_setting") + "?tab=my_pet",
        'env_photos': Img.objects.filter(type="env_photo").filter(be_used=True).filter(owner_sitter=sitter),
        'post_url': reverse("img:upload_env_photo", kwargs={'sitter_id': sitter.pk}),
        'active_tab': request.GET.get("tab", "personal"),
    }
    ctx.update(csrf(request))
    return render(request, 'sitter/manage/setting.html', ctx)


def common_setting(request, form_class):
    if request.POST:
        form = form_class(request.POST, instance=request.user.userinfo.sitter)
        if form.is_valid():
            form.save()
    return redirect("sitter:manage")


@login_required
def manage_setting_info(request):
    return common_setting(request, SitterInfoForm)


@login_required
def manage_setting_address(request):
    return common_setting(request, SitterAddressForm)


@login_required
def manage_setting_pet(request):
    if request.POST:
        form = SitterPetForm(request.POST, instance=request.user.userinfo.sitter)
        if form.is_valid():
            info = form.save(commit=False)
            # 特色服务
            special_services = request.POST.getlist("special_services")
            info.special_service = ",".join(special_services)
            info.save()
    return redirect("sitter:manage")


@login_required
def manage_setting_my_pet(request):
    return common_setting(request, SitterMyPetForm)


@login_required
def manage_setting_photo(request):
    return common_setting(request, SitterInfoForm)


# 设置日期
@login_required
def manage_date(request):
    sitter = request.user.userinfo.sitter
    if request.POST:
        date_list = request.POST.get("date_list", "")
        sitter.cant_date = date_list
        sitter.save()
        return redirect("sitter:manage")
    ctx = {
        'sitter': sitter
    }
    ctx.update(csrf(request))
    return render(request, 'sitter/manage/date.html', ctx)


@login_required
def manage_order(request):
    ctx = {
        'user_info': request.user.userinfo,
        'sitter': request.user.userinfo.sitter,
    }
    return render(request, 'sitter/manage/order.html', ctx)


@login_required
def manage_photo(request):
    ctx = {
        'user_info': request.user.userinfo,
        'sitter': request.user.userinfo.sitter,
    }
    return render(request, 'sitter/manage/photo.html', ctx)
