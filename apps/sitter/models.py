# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from datetime import timedelta

from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_delete, pre_save, post_init
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.staticfiles.templatetags.staticfiles import static

from apps.common import manage_head_portrait
from apps.pet.models import PetTypeInfo, pet_type_choices
from apps.sitter.utils import get_distance

__author__ = 'peter'

price_name_list = [
    'pup',
    'small_dog',
    'medium_dog',
    'big_dog',
    'cat',
    'others',
]


class SitterPetPriceInfo(PetTypeInfo):
    price_name = ''
    price_name_desc = ''
    price_enable_name = ''
    price_enable_desc = ''

    def set_price_info(self, info_id, name, desc, price_name):
        super(SitterPetPriceInfo, self).set_info(info_id, name, desc)
        self.price_name = price_name + '_price'
        self.price_name_desc = name + '价格'
        self.price_enable_name = self.price_name + '_enable'
        self.price_enable_desc = '选中' + self.price_name_desc


# 初始化宠物价格信息数组
sitter_pet_price_info_list = []
for i, pet_type in enumerate(pet_type_choices):
    info = SitterPetPriceInfo()
    info.set_price_info(i, pet_type[0], pet_type[1], price_name_list[i])
    sitter_pet_price_info_list.append(info)


@python_2_unicode_compatible
class Sitter(models.Model):
    sitter_type_choices = (
        ("寄养师", "寄养师"),
        ("宠物店", "宠物店"),
        ("寄养场", "寄养场"),
    )
    status_choices = (
        ("等待认证", "等待认证"),
        ("认证成功", "认证成功"),
    )
    sex_choices = (
        ("男", "男"),
        ("女", "女"),
    )
    province_choices = (
        ("北京", "北京"),
        ("天津", "天津"),
        ("河北", "河北"),
        ("山西", "山西"),
        ("内蒙古", "内蒙古"),
        ("辽宁", "辽宁"),
        ("吉林", "吉林"),
        ("黑龙江", "黑龙江"),
        ("上海", "上海"),
        ("江苏", "江苏"),
        ("浙江", "浙江"),
        ("安徽", "安徽"),
        ("福建", "福建"),
        ("江西", "江西"),
        ("山东", "山东"),
        ("河南", "河南"),
        ("湖北", "湖北"),
        ("湖南", "湖南"),
        ("广东", "广东"),
        ("海南", "海南"),
        ("广西", "广西"),
        ("甘肃", "甘肃"),
        ("陕西", "陕西"),
        ("新疆", "新疆"),
        ("青海", "青海"),
        ("宁夏", "宁夏"),
        ("重庆", "重庆"),
        ("四川", "四川"),
        ("贵州", "贵州"),
        ("云南", "云南"),
        ("西藏", "西藏"),
        ("台湾", "台湾"),
        ("澳门", "澳门"),
        ("香港", "香港"),
        ("国外", "国外"),
    )
    pet_price_enable_choices = (
        (0, "否"),
        (1, "是"),
    )
    shop_size_choices = (
        ("小", "小型 1-10只"),
        ("中", "中型 10-50只"),
        ("大", "大型 50只以上"),
    )
    field_size_choices = (
        ("小", "小型 1-50只"),
        ("中", "中型 50-100只"),
        ("大", "大型 100只以上"),
    )
    # 管理员
    manager = models.ForeignKey("user.UserInfo", related_name="manager")
    # 类型(宠物店,专业寄养点,家庭寄养师)
    sitter_type = models.CharField("类型", choices=sitter_type_choices, max_length=20, default='宠物店')
    # 昵称
    nickname = models.CharField("昵称", max_length=50)
    # 联系电话
    phone = models.CharField("联系电话", max_length=20, blank=True)
    # 状态
    status = models.CharField("状态", choices=status_choices, max_length=20, default="等待认证")
    # 个人描述
    desc = models.TextField("个人描述", max_length=500, blank=True)

    # 真实姓名
    realname = models.CharField("真实姓名", max_length=50, blank=True)
    # 性别
    sex = models.CharField("性别", choices=sex_choices, max_length=10, default="男")
    # 邮箱
    email = models.EmailField("邮箱", max_length=50, blank=True)
    # 身份证号
    id_number = models.CharField("身份证号", max_length=30, blank=True)
    # 组织机构代码
    group_number = models.CharField("组织机构代码", max_length=50, blank=True)
    # 职业
    occupation = models.CharField("职业", max_length=20, blank=True)

    # 保存经纬度信息
    longitude = models.FloatField("经度", blank=True)
    latitude = models.FloatField("纬度", blank=True)

    # 省份
    province = models.CharField("省份", choices=province_choices, max_length=50, default="成都")
    # 城市
    city = models.CharField("城市", max_length=50, blank=True)
    # 区/县
    county = models.CharField("区县", max_length=50, blank=True)
    # 街道/小区
    street = models.CharField("街道/小区", max_length=50, blank=True)
    # 楼,单元,门牌
    house_number = models.CharField("楼,单元,门牌", max_length=50, blank=True)
    # 房间面积
    space = models.IntegerField("房间面积", blank=True, default=100)
    # 宠物店规模
    shop_size = models.CharField("宠物店规模", choices=shop_size_choices, max_length=20, default="小")
    # 寄养场规模
    field_size = models.CharField("寄养场规模", choices=field_size_choices, max_length=20, default="小")

    # 饲养经验
    experience = models.IntegerField("饲养经验", default=0)

    # 特色服务
    special_service = models.CharField("特色服务", max_length=100, blank=True)
    # 头像
    head_portrait = models.OneToOneField("img.Img", verbose_name="头像", null=True, blank=True, on_delete=models.SET_NULL)
    # 星级
    star = models.IntegerField("星级", default=0)

    # 推荐人电话
    referee_phone = models.CharField("推荐人电话", max_length=20, blank=True)

    # 不接受寄养日期
    cant_date = models.TextField("不接受寄养日期", max_length=800, blank=True)

    def _get_min_price(self):
        price_list = []
        for price in self.pet_price_list:
            if price[0]:
                price_list.append(price[1])

        if len(price_list) == 0:
            return 0
        return min(price_list)

    # 最低价格
    min_price = property(_get_min_price)

    def _get_head_portrait_url(self):
        if self.head_portrait:
            return self.head_portrait.img.url
        elif self.sex == '男':
            return static("images/head_portrait/male.png")
        else:
            return static("images/head_portrait/female.png")

    # 显示用的头像路径,包含默认值
    head_portrait_url = property(_get_head_portrait_url)

    # 获得单个寄养师
    @staticmethod
    def get(pk):
        return Sitter.objects.get(pk=pk)

    # 获得所有寄养师
    @staticmethod
    def get_all():
        # TODO 添加认证成功限定
        # return Sitter.objects.filter(status="认证成功")
        return Sitter.objects.all()

    # 搜索昵称或地址
    @staticmethod
    def search_nickname_or_address(queryset, search_value):
        if len(search_value) > 0:
            queryset = queryset.filter(
                Q(nickname__contains=search_value) |
                Q(city__contains=search_value) |
                Q(county__contains=search_value) |
                Q(street__contains=search_value) |
                Q(house_number__contains=search_value))
        return queryset

    # 按寄养日期来过滤寄养师
    @staticmethod
    def filter_date(queryset, beg_date, end_date):
        if beg_date and end_date:
            delta = end_date - beg_date
            for i in range(delta.days + 1):
                exclude_date = beg_date + timedelta(days=i)
                queryset = queryset.exclude(cant_date__contains=exclude_date)
        return queryset

    # 按寄养师类型来过滤寄养师
    @staticmethod
    def filter_sitter_type(queryset, sitter_type):
        if sitter_type:
            queryset = queryset.filter(sitter_type=sitter_type)
        return queryset

    # 按宠物类型过滤寄养师
    @staticmethod
    def filter_pet_type(queryset, pet_type):
        if pet_type:
            if pet_type == '幼犬':
                queryset = queryset.filter(pup_price_enable=1)
            elif pet_type == '小型犬':
                queryset = queryset.filter(small_dog_price_enable=1)
            elif pet_type == '中型犬':
                queryset = queryset.filter(medium_dog_price_enable=1)
            elif pet_type == '大型犬':
                queryset = queryset.filter(big_dog_price_enable=1)
            elif pet_type == '猫咪':
                queryset = queryset.filter(cat_price_enable=1)
            elif pet_type == '其他':
                queryset = queryset.filter(others_price_enable=1)
        return queryset

    # 按距离排序
    # TODO 按距离排序,现在用户少,可以用粗暴的排序方式,以后有主机了一定要改为GeoDjango的方式
    @staticmethod
    def sort_by_distance(queryset, longitude, latitude):
        if longitude and latitude:
            # 开始排序
            queryset = sorted(queryset,
                              key=lambda sitter: get_distance(
                                  float(longitude), float(latitude), sitter.longitude, sitter.latitude))
        return queryset

    def __str__(self):
        return self.nickname


# 通过宠物价格信息来添加价格字段和是否选中价格字段
for info in sitter_pet_price_info_list:
    Sitter.add_to_class(info.price_name, models.IntegerField(info.price_name_desc, default=50))
    Sitter.add_to_class(info.price_enable_name,
                        models.IntegerField(info.price_enable_desc, choices=Sitter.pet_price_enable_choices, default=0))


# 当寄养师被查询后设置价格和是否选中数组
@receiver(post_init, sender=Sitter)
def on_init(sender, instance, **kwargs):
    instance.pet_price_list = [
        (instance.pup_price_enable, instance.pup_price, sitter_pet_price_info_list[0]),
        (instance.small_dog_price_enable, instance.small_dog_price, sitter_pet_price_info_list[1]),
        (instance.medium_dog_price_enable, instance.medium_dog_price, sitter_pet_price_info_list[2]),
        (instance.big_dog_price_enable, instance.big_dog_price, sitter_pet_price_info_list[3]),
        (instance.cat_price_enable, instance.cat_price, sitter_pet_price_info_list[4]),
        (instance.others_price_enable, instance.others_price, sitter_pet_price_info_list[5])]


# 当寄养师被删除时同时删除其对应的头像
@receiver(pre_delete, sender=Sitter)
def on_delete(sender, instance, **kwargs):
    manage_head_portrait.on_delete(sender, instance, **kwargs)


# 当寄养师被保存时判断头像的变化情况
@receiver(pre_save, sender=Sitter)
def on_save(sender, instance, **kwargs):
    manage_head_portrait.on_save(sender, instance, **kwargs)


# 寄养师可以设置的特色服务
@python_2_unicode_compatible
class SitterSpecialService(models.Model):
    text = models.CharField("标签文本", max_length=20, unique=True)
    enable = models.BooleanField("有效", default=True)
    img = models.CharField("图标", max_length=20)

    def _get_img_url(self):
        return static("images/special_service/%s.png" % self.img)

    img_url = property(_get_img_url)

    def __str__(self):
        return self.text
