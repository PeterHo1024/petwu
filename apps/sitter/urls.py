# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.conf.urls import url

from . import views

__author__ = 'peter'

urlpatterns = [
    url(r'^add/$', views.add, name='add'),

    url(r'^$', views.list_sitters, name='list'),
    url(r'^page/(?P<page>[0-9]+)/$', views.list_sitters_page, name='page'),
    url(r'^(?P<pk>[0-9]+)/$', views.detail, name='detail'),

    url(r'^manage/$', views.manage, name='manage'),
    url(r'^manage/setting/$', views.manage_setting, name='manage_setting'),
    url(r'^manage/setting/info/$', views.manage_setting_info, name='manage_setting_info'),
    url(r'^manage/setting/address/$', views.manage_setting_address, name='manage_setting_address'),
    url(r'^manage/setting/pet/$', views.manage_setting_pet, name='manage_setting_pet'),
    url(r'^manage/setting/my_pet/$', views.manage_setting_my_pet, name='manage_setting_my_pet'),
    url(r'^manage/setting/photo/$', views.manage_setting_photo, name='manage_setting_photo'),
    url(r'^manage/date/$', views.manage_date, name='manage_date'),
    url(r'^manage/order/$', views.manage_order, name='manage_order'),
    url(r'^manage/photo/$', views.manage_photo, name='manage_photo'),
]
