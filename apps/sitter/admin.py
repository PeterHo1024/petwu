# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.contrib import admin
from .models import Sitter, SitterSpecialService

__author__ = 'peter'


class SitterAdmin(admin.ModelAdmin):
    list_display = (
        'nickname',
        'phone',
        'status',
        'sitter_type',
    )
    search_fields = (
        'nickname',
    )
    list_filter = (
        'status',
        'sitter_type',
    )
    fieldsets = [
        ('常用信息:', {
            'fields': [
                'manager',
                'nickname',
                'phone',
                'status',
                'sitter_type',
                'desc',
            ]}),
        ('隐私信息:', {
            'classes': ['collapse', ],
            'fields': [
                'realname',
                'sex',
                'email',
                'id_number',
                'group_number',
                'occupation',
            ]}),
        ('地址信息:', {
            'classes': ['collapse', ],
            'fields': [
                'longitude',
                'latitude',
                'province',
                'city',
                'county',
                'street',
                'house_number',
                'space',
                'shop_size',
                'field_size',
            ]}),
        ('宠物信息:', {
            'classes': ['collapse', ],
            'fields': [
                'experience',
                'pup_price',
                'pup_price_enable',
                'small_dog_price',
                'small_dog_price_enable',
                'medium_dog_price',
                'medium_dog_price_enable',
                'big_dog_price',
                'big_dog_price_enable',
                'cat_price',
                'cat_price_enable',
                'others_price',
                'others_price_enable',
            ]}),
        ('其他信息:', {
            'classes': ['collapse', ],
            'fields': [
                'special_service',
                'head_portrait',
                'star',
                'cant_date',
            ]}),
    ]


admin.site.register(Sitter, SitterAdmin)
admin.site.register(SitterSpecialService)
