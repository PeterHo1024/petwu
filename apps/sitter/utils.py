# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

__author__ = 'peter'


# 返回两个坐标之间的距离
def get_distance(x1, y1, x2, y2):
    return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)
