# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitter', '0005_auto_20160224_1745'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitter',
            name='latitude',
            field=models.FloatField(default=104, verbose_name='纬度', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='sitter',
            name='longitude',
            field=models.FloatField(default=30, verbose_name='经度', blank=True),
            preserve_default=False,
        ),
    ]
