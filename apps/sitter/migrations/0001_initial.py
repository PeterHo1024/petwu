# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('img', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sitter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sitter_type', models.CharField(default='\u5ba0\u7269\u5e97', max_length=20, verbose_name='\u7c7b\u578b', choices=[('\u5bc4\u517b\u5e08', '\u5bc4\u517b\u5e08'), ('\u5ba0\u7269\u5e97', '\u5ba0\u7269\u5e97'), ('\u5bc4\u517b\u573a', '\u5bc4\u517b\u573a')])),
                ('nickname', models.CharField(max_length=50, verbose_name='\u6635\u79f0')),
                ('phone', models.CharField(max_length=20, verbose_name='\u8054\u7cfb\u7535\u8bdd', blank=True)),
                ('status', models.CharField(default='\u7b49\u5f85\u8ba4\u8bc1', max_length=20, verbose_name='\u72b6\u6001', choices=[('\u7b49\u5f85\u8ba4\u8bc1', '\u7b49\u5f85\u8ba4\u8bc1'), ('\u8ba4\u8bc1\u6210\u529f', '\u8ba4\u8bc1\u6210\u529f')])),
                ('desc', models.TextField(max_length=500, verbose_name='\u4e2a\u4eba\u63cf\u8ff0', blank=True)),
                ('realname', models.CharField(max_length=50, verbose_name='\u771f\u5b9e\u59d3\u540d', blank=True)),
                ('sex', models.CharField(default='\u7537', max_length=10, verbose_name='\u6027\u522b', choices=[('\u7537', '\u7537'), ('\u5973', '\u5973')])),
                ('email', models.EmailField(max_length=50, verbose_name='\u90ae\u7bb1', blank=True)),
                ('id_number', models.CharField(max_length=30, verbose_name='\u8eab\u4efd\u8bc1\u53f7', blank=True)),
                ('group_number', models.CharField(max_length=50, verbose_name='\u7ec4\u7ec7\u673a\u6784\u4ee3\u7801', blank=True)),
                ('occupation', models.CharField(max_length=20, verbose_name='\u804c\u4e1a', blank=True)),
                ('province', models.CharField(default='\u6210\u90fd', max_length=50, verbose_name='\u7701\u4efd', choices=[('\u5317\u4eac', '\u5317\u4eac'), ('\u5929\u6d25', '\u5929\u6d25'), ('\u6cb3\u5317', '\u6cb3\u5317'), ('\u5c71\u897f', '\u5c71\u897f'), ('\u5185\u8499\u53e4', '\u5185\u8499\u53e4'), ('\u8fbd\u5b81', '\u8fbd\u5b81'), ('\u5409\u6797', '\u5409\u6797'), ('\u9ed1\u9f99\u6c5f', '\u9ed1\u9f99\u6c5f'), ('\u4e0a\u6d77', '\u4e0a\u6d77'), ('\u6c5f\u82cf', '\u6c5f\u82cf'), ('\u6d59\u6c5f', '\u6d59\u6c5f'), ('\u5b89\u5fbd', '\u5b89\u5fbd'), ('\u798f\u5efa', '\u798f\u5efa'), ('\u6c5f\u897f', '\u6c5f\u897f'), ('\u5c71\u4e1c', '\u5c71\u4e1c'), ('\u6cb3\u5357', '\u6cb3\u5357'), ('\u6e56\u5317', '\u6e56\u5317'), ('\u6e56\u5357', '\u6e56\u5357'), ('\u5e7f\u4e1c', '\u5e7f\u4e1c'), ('\u6d77\u5357', '\u6d77\u5357'), ('\u5e7f\u897f', '\u5e7f\u897f'), ('\u7518\u8083', '\u7518\u8083'), ('\u9655\u897f', '\u9655\u897f'), ('\u65b0\u7586', '\u65b0\u7586'), ('\u9752\u6d77', '\u9752\u6d77'), ('\u5b81\u590f', '\u5b81\u590f'), ('\u91cd\u5e86', '\u91cd\u5e86'), ('\u56db\u5ddd', '\u56db\u5ddd'), ('\u8d35\u5dde', '\u8d35\u5dde'), ('\u4e91\u5357', '\u4e91\u5357'), ('\u897f\u85cf', '\u897f\u85cf'), ('\u53f0\u6e7e', '\u53f0\u6e7e'), ('\u6fb3\u95e8', '\u6fb3\u95e8'), ('\u9999\u6e2f', '\u9999\u6e2f'), ('\u56fd\u5916', '\u56fd\u5916')])),
                ('city', models.CharField(max_length=50, verbose_name='\u57ce\u5e02', blank=True)),
                ('county', models.CharField(max_length=50, verbose_name='\u533a\u53bf', blank=True)),
                ('street', models.CharField(max_length=50, verbose_name='\u8857\u9053/\u5c0f\u533a', blank=True)),
                ('house_number', models.CharField(max_length=50, verbose_name='\u697c,\u5355\u5143,\u95e8\u724c', blank=True)),
                ('space', models.IntegerField(default=100, verbose_name='\u623f\u95f4\u9762\u79ef', blank=True)),
                ('shop_size', models.CharField(default='\u5c0f', max_length=20, verbose_name='\u5ba0\u7269\u5e97\u89c4\u6a21', choices=[('\u5c0f', '\u5c0f\u578b 1-10\u53ea'), ('\u4e2d', '\u4e2d\u578b 10-50\u53ea'), ('\u5927', '\u5927\u578b 50\u53ea\u4ee5\u4e0a')])),
                ('field_size', models.CharField(default='\u5c0f', max_length=20, verbose_name='\u5bc4\u517b\u573a\u89c4\u6a21', choices=[('\u5c0f', '\u5c0f\u578b 1-50\u53ea'), ('\u4e2d', '\u4e2d\u578b 50-100\u53ea'), ('\u5927', '\u5927\u578b 100\u53ea\u4ee5\u4e0a')])),
                ('experience', models.IntegerField(default=0, verbose_name='\u9972\u517b\u7ecf\u9a8c')),
                ('pup_price', models.IntegerField(default=50, verbose_name='\u5e7c\u72ac\u4ef7\u683c')),
                ('pup_price_enable', models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u5e7c\u72ac\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')])),
                ('small_dog_price', models.IntegerField(default=40, verbose_name='\u5c0f\u578b\u72ac\u4ef7\u683c')),
                ('small_dog_price_enable', models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u5c0f\u578b\u72ac\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')])),
                ('medium_dog_price', models.IntegerField(default=50, verbose_name='\u4e2d\u578b\u72ac\u4ef7\u683c')),
                ('medium_dog_price_enable', models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u4e2d\u578b\u72ac\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')])),
                ('big_dog_price', models.IntegerField(default=100, verbose_name='\u5927\u578b\u72ac\u4ef7\u683c')),
                ('big_dog_price_enable', models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u5927\u578b\u72ac\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')])),
                ('cat_price', models.IntegerField(default=50, verbose_name='\u732b\u54aa\u4ef7\u683c')),
                ('cat_price_enable', models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u732b\u54aa\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')])),
                ('others_price', models.IntegerField(default=50, verbose_name='\u5176\u4ed6\u4ef7\u683c')),
                ('others_price_enable', models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u5176\u4ed6\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')])),
                ('special_service', models.CharField(max_length=100, verbose_name='\u7279\u8272\u670d\u52a1', blank=True)),
                ('star', models.IntegerField(default=0, verbose_name='\u661f\u7ea7')),
                ('referee_phone', models.CharField(max_length=20, verbose_name='\u63a8\u8350\u4eba\u7535\u8bdd', blank=True)),
                ('cant_date', models.TextField(max_length=800, verbose_name='\u4e0d\u63a5\u53d7\u5bc4\u517b\u65e5\u671f', blank=True)),
                ('head_portrait', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, blank=True, to='img.Img', verbose_name='\u5934\u50cf')),
            ],
        ),
        migrations.CreateModel(
            name='SitterSpecialService',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(unique=True, max_length=20, verbose_name='\u6807\u7b7e\u6587\u672c')),
                ('enable', models.BooleanField(default=True, verbose_name='\u6709\u6548')),
                ('img', models.CharField(max_length=20, verbose_name='\u56fe\u6807')),
            ],
        ),
    ]
