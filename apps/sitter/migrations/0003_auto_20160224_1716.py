# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitter', '0002_sitter_manager'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sitter',
            name='big_dog_price',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='big_dog_price_enable',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='cat_price',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='cat_price_enable',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='medium_dog_price',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='medium_dog_price_enable',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='others_price',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='others_price_enable',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='pup_price',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='pup_price_enable',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='small_dog_price',
        ),
        migrations.RemoveField(
            model_name='sitter',
            name='small_dog_price_enable',
        ),
    ]
