# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitter', '0004_auto_20160224_1723'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sitter',
            name='big_dog_price',
            field=models.IntegerField(default=50, verbose_name='\u5927\u578b\u72ac\u4ef7\u683c'),
        ),
        migrations.AlterField(
            model_name='sitter',
            name='small_dog_price',
            field=models.IntegerField(default=50, verbose_name='\u5c0f\u578b\u72ac\u4ef7\u683c'),
        ),
    ]
