# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitter', '0001_initial'),
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitter',
            name='manager',
            field=models.ForeignKey(related_name='manager', to='user.UserInfo'),
        ),
    ]
