# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitter', '0003_auto_20160224_1716'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitter',
            name='big_dog_price',
            field=models.IntegerField(default=100, verbose_name='\u5927\u578b\u72ac\u4ef7\u683c'),
        ),
        migrations.AddField(
            model_name='sitter',
            name='big_dog_price_enable',
            field=models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u5927\u578b\u72ac\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')]),
        ),
        migrations.AddField(
            model_name='sitter',
            name='cat_price',
            field=models.IntegerField(default=50, verbose_name='\u732b\u54aa\u4ef7\u683c'),
        ),
        migrations.AddField(
            model_name='sitter',
            name='cat_price_enable',
            field=models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u732b\u54aa\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')]),
        ),
        migrations.AddField(
            model_name='sitter',
            name='medium_dog_price',
            field=models.IntegerField(default=50, verbose_name='\u4e2d\u578b\u72ac\u4ef7\u683c'),
        ),
        migrations.AddField(
            model_name='sitter',
            name='medium_dog_price_enable',
            field=models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u4e2d\u578b\u72ac\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')]),
        ),
        migrations.AddField(
            model_name='sitter',
            name='others_price',
            field=models.IntegerField(default=50, verbose_name='\u5176\u4ed6\u4ef7\u683c'),
        ),
        migrations.AddField(
            model_name='sitter',
            name='others_price_enable',
            field=models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u5176\u4ed6\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')]),
        ),
        migrations.AddField(
            model_name='sitter',
            name='pup_price',
            field=models.IntegerField(default=50, verbose_name='\u5e7c\u72ac\u4ef7\u683c'),
        ),
        migrations.AddField(
            model_name='sitter',
            name='pup_price_enable',
            field=models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u5e7c\u72ac\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')]),
        ),
        migrations.AddField(
            model_name='sitter',
            name='small_dog_price',
            field=models.IntegerField(default=40, verbose_name='\u5c0f\u578b\u72ac\u4ef7\u683c'),
        ),
        migrations.AddField(
            model_name='sitter',
            name='small_dog_price_enable',
            field=models.IntegerField(default=0, verbose_name='\u9009\u4e2d\u5c0f\u578b\u72ac\u4ef7\u683c', choices=[(0, '\u5426'), (1, '\u662f')]),
        ),
    ]
