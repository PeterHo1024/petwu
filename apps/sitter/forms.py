# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.forms import Form, ModelForm, RadioSelect, Select, TextInput, Textarea, ChoiceField, DateField
from django.utils.translation import ugettext_lazy as _
from apps.img.models import Img
from apps.pet.models import pet_type_choices
from .models import Sitter

__author__ = 'peter'


class SitterFormBaseMeta:
    model = Sitter
    widgets = {
        'sitter_type': RadioSelect(choices=Sitter.sitter_type_choices),
        'desc': Textarea(attrs={'placeholder': '选填,限500个字符以内'}),
        'city': Select(),
        'referee_phone': TextInput(attrs={'placeholder': '推荐人电话,选填'}),
    }
    labels = {
        'nickname': _('名称'),
        'desc': _('介绍'),
        'realname': _('姓名'),
        'pup_price': _('幼犬'),
        'small_dog_price': _('小型犬'),
        'medium_dog_price': _('中型犬'),
        'big_dog_price': _('大型犬'),
        'cat_price': _('猫咪'),
        'others_price': _('其他'),
        'special_service': _('其他特色'),
    }
    help_texts = {
    }
    error_messages = [
    ]

    def __init__(self):
        pass


class SitterForm(ModelForm):
    class Meta(SitterFormBaseMeta):
        exclude = [
            'manager',
            'status',
            'star',
        ]

    def save(self, commit=True, user_info=None, special_services=None):
        # 保存寄养师信息
        new_sitter = super(SitterForm, self).save(commit=False)
        if user_info:
            new_sitter.manager = user_info
        # 特色服务
        if special_services:
            new_sitter.special_service = ",".join(special_services)
        new_sitter.save(commit)
        return new_sitter


class SitterEnvPhotoForm(ModelForm):
    class Meta:
        model = Img
        exclude = [
            'sitter',
            'type',
        ]


class SitterInfoForm(ModelForm):
    class Meta(SitterFormBaseMeta):
        fields = (
            'head_portrait',
            'desc',
            'occupation',
            'experience',
            'phone',
            'email',
            'space',
            'shop_size',
            'field_size',
        )


class SitterAddressForm(ModelForm):
    class Meta(SitterFormBaseMeta):
        fields = (
            'province',
            'city',
            'county',
            'street',
            'house_number',
        )


class SitterPetForm(ModelForm):
    class Meta(SitterFormBaseMeta):
        fields = (
            'pup_price',
            'pup_price_enable',
            'small_dog_price',
            'small_dog_price_enable',
            'medium_dog_price',
            'medium_dog_price_enable',
            'big_dog_price',
            'big_dog_price_enable',
            'cat_price',
            'cat_price_enable',
            'others_price',
            'others_price_enable',
            'special_service',
        )


class SitterMyPetForm(ModelForm):
    class Meta(SitterFormBaseMeta):
        fields = (
        )


class SitterFilterForm(Form):
    beg_date = DateField(required=False)
    end_date = DateField(required=False)
    pet_type = ChoiceField(label="宠物类型", choices=pet_type_choices, required=False)
    sitter_type = ChoiceField(label="寄养师类型", choices=Sitter.sitter_type_choices, required=False)
