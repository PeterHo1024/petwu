# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

import urllib

from apps.weixin.utils import APPID, WeChat

__author__ = 'peter'

# 如果要修改这个地址,需要在 微信公众平台->开发->接口权限->网页授权获取用户基本信息 处修改授权回调页面域名
base_url = 'https://petwuapp.duapp.com/'
base_url_http = base_url.replace('https', 'http')

auth_redirect_uri = urllib.quote_plus(base_url + 'petwu/weixin/auth/')
auth_url = "https://open.weixin.qq.com/connect/oauth2/authorize?" \
           "appid={APPID}&" \
           "redirect_uri={REDIRECT_URI}&" \
           "response_type={RESPONSE_TYPE}&" \
           "scope={SCOPE}&" \
           "state={STATE}#wechat_redirect"

master_auth_url = auth_url.format(
    APPID=APPID,
    REDIRECT_URI=auth_redirect_uri,
    RESPONSE_TYPE='code',
    SCOPE='snsapi_userinfo',
    STATE='master',
)

sitter_auth_url = auth_url.format(
    APPID=APPID,
    REDIRECT_URI=auth_redirect_uri,
    RESPONSE_TYPE='code',
    SCOPE='snsapi_userinfo',
    STATE='sitter',
)

get_location_url = base_url_http + 'petwu/weixin/location/'

print('master_auth_url: ' + master_auth_url)
print('sitter_auth_url: ' + sitter_auth_url)
print('get_location_url: ' + get_location_url)

menu = {
    "button": [{
        "type": "view",
        "name": "我要寄养",
        "url": master_auth_url,
    }, {
        "type": "view",
        "name": "我是寄养师",
        "url": sitter_auth_url,
    }, {
        "name": "菜单",
        "sub_button": [{
            "type": "view",
            "name": "获取当前位置",
            "url": get_location_url
        }, {
            "type": "view",
            "name": "视频",
            "url": "http://v.qq.com/"
        }, {
            "type": "click",
            "name": "赞一下我们",
            "key": "V1001_GOOD"
        }],
    }],
}
WeChat().set_wechat_menu(menu)
