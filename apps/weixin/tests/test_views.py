# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.contrib.auth import get_user_model
from django.http import HttpRequest
from django.test import TestCase, Client

from mock import Mock

from apps.user.models import UserInfo
from apps.weixin.views import create_user_and_login

__author__ = 'peter'

User = get_user_model()


class WeChatViewTest(TestCase):
    def setUp(self):
        self.wechat = Mock()
        self.wechat.nickname = 'test_nickname'
        self.wechat.openid = 'test_openid'
        self.wechat.sex = 1

        self.client = Client()
        self.request = HttpRequest()
        self.request.session = self.client.session

    def test_create_user_and_login_by_wechat_and_no_user(self):
        user = create_user_and_login(self.request, self.wechat)

        self.assertEqual(user.userinfo.username, self.wechat.openid)
        self.assertEqual(user.userinfo.openid, self.wechat.openid)
        self.assertEqual(user.userinfo.master.nickname, self.wechat.nickname)

    def test_create_user_and_login_by_wechat_and_has_user(self):
        username = 'username1'
        password = 'password1'
        user_info = UserInfo.create_user(username, password)
        user_info.openid = "test_openid"
        user_info.save()

        user = create_user_and_login(self.request, self.wechat)

        self.assertEqual(user.userinfo.username, username)
