# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.contrib.auth import login
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from apps.user.models import UserInfo
from apps.weixin.utils import APPID, get_wechat

__author__ = 'peter'


def handle_get_request(request):
    wechat = get_wechat()
    signature = request.GET.get("signature", None)
    timestamp = request.GET.get("timestamp", None)
    nonce = request.GET.get("nonce", None)

    if wechat.check_signature(signature=signature, timestamp=timestamp, nonce=nonce):
        return HttpResponse(request.GET.get("echostr", None))


def handle_post_request(request):
    wechat = get_wechat()
    body_text = request.body
    if type(body_text) == str:
        body_text = body_text.decode('utf-8')
    wechat.parse_data(body_text)
    message = wechat.get_message()

    if message.type == 'text':
        if message.content == 'hi':
            user_info = wechat.get_user_info(user_id=wechat.openid)
            response = wechat.response_text(user_info['nickname'])
        else:
            response = wechat.response_text('文字')
    elif message.type == 'image':
        response = wechat.response_text('图片')
    elif message.type == 'location':
        response = wechat.response_text("%f, %f, %f" % (message.longitude, message.latitude, message.precision))
    else:
        response = wechat.response_text(message.type)
    return HttpResponse(response, content_type="application/xml")


@csrf_exempt
def handle_request(request):
    if request.method == 'GET':
        return handle_get_request(request)
    elif request.method == 'POST':
        return handle_post_request(request)
    else:
        return None


# 获得用户位置信息
def location(request):
    wechat = get_wechat()
    timestamp = 1414587457
    nonce_str = 'Wm3WZYTPz0wzccnW'
    url = request.build_absolute_uri()
    signature = wechat.generate_jsapi_signature(timestamp, nonce_str, url)
    ctx = {
        'app_id': APPID,
        'timestamp': timestamp,
        'nonce_str': nonce_str,
        'signature': signature,
        'wechat': wechat,
    }
    return render(request, 'weixin/location.html', ctx)


# 创建用户并登陆
def create_user_and_login(request, wechat):
    # 判断用户是否存在
    user_info = UserInfo.objects.filter(openid=wechat.openid)
    if len(user_info) > 0:
        # 如果存在,就直接登陆
        user_info = user_info[0]
        user = user_info.user
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)
        return user
    else:
        # 创建用户
        # TODO 用户名的生成也改得稍微好看点吧,比如wechat_xxxx接数字,随机生成密码
        # 创建User,UserInfo和Master
        user_info = UserInfo.create_user(wechat.openid, wechat.openid)
        master = user_info.master
        # 设置user_info和master的其他信息
        user_info.openid = wechat.openid
        user_info.save()
        master.nickname = wechat.nickname
        if wechat.sex == 1:
            master.sex = '男'
        elif wechat.sex == 2:
            master.sex = '女'
        master.save()
        # 登陆
        user_info.user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user_info.user)
        return user_info.user


# 处理用户认证
@csrf_exempt
def auth(request):
    code = request.GET.get('code', None)
    state = request.GET.get('state', None)
    wechat = get_wechat()
    if wechat.auth(code) is False:
        # 失败
        return HttpResponse("用户认证失败")
    # 通过openid创建用户并登陆
    create_user_and_login(request, wechat)
    # 根据state来重定向
    if state == 'master':
        return redirect('master:home')
    elif state == 'sitter':
        return redirect('sitter:manage')


# 登陆,仅供测试使用
def login_for_test(request):
    return render(request, "weixin/login.html")


# 测试用
def json_for_test(request):
    # f = open('/home/peter/Downloads/userinfo', 'rb')
    # info = f.read()
    # f.close()
    # return HttpResponse(info)
    return HttpResponse(
        """
<html>
<head>
<link title="Wrap Long Lines" href="resource://gre-resources/plaintext.css" type="text/css" rel="alternate stylesheet">
</head>
<body>
<pre>
{"openid":"oh3syt1hAXqEb_JhRaKpE929BRIQ","nickname":"one","sex":1,"language":"zh_CN","city":"成都","province":"四川","country":"中国","headimgurl":"http:\/\/wx.qlogo.cn\/mmopen\/ryxcjBPmcGuOg6EG9ll3Xwf2toTJky7bcBSAyjXNMaE5iaJgAicCuldibaoAsRD9vacrX6adiajicF5x18U7KV4YricHnic1X3t00oY\/0","privilege":[]}
</pre>
</body>
</html>
"""
    )
