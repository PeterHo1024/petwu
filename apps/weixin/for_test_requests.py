# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

import requests

__author__ = 'peter'

server_url = "https://petwuapp.duapp.com/"
local_url = "http://localhost:8000/"

body_text = """
    <xml>
    <ToUserName><![CDATA[touser]]></ToUserName>
    <FromUserName><![CDATA[fromuser]]></FromUserName>
    <CreateTime>1405994593</CreateTime>
    <MsgType><![CDATA[text]]></MsgType>
    <Content><![CDATA[wechat]]></Content>
    <MsgId>6038700799783131222</MsgId>
    </xml>
    """

body_location = """
    <xml>
    <ToUserName><![CDATA[toUser]]></ToUserName>
    <FromUserName><![CDATA[fromUser]]></FromUserName>
    <CreateTime>123456789</CreateTime>
    <MsgType><![CDATA[event]]></MsgType>
    <Event><![CDATA[LOCATION]]></Event>
    <Latitude>23.137466</Latitude>
    <Longitude>113.352425</Longitude>
    <Precision>119.385040</Precision>
    </xml>
"""


def test_handle_post_request():
    param = 'signature=630188a53858b2c406ba99a2ab879be120afe187&timestamp=1453968916&nonce=933419921'
    response = requests.post(local_url + "petwu/weixin/?" + param, data=body_location)
    print(response.text)


test_handle_post_request()


def test_request_json():
    url = "http://127.0.0.1/userinfo.html"
    response = requests.get(url)
    response.encoding = 'utf-8'
    response = response.json()
    __openid = response['openid']
    __nickname = response['nickname']
    __sex = response['sex']
    __province = response['province']
    __city = response['city']
    __country = response['country']
    __headimgurl = response['headimgurl']
    __privilege = response['privilege']

# test_request_json()
