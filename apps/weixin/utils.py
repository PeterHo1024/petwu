# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

import requests

from wechat_sdk import WechatBasic
from wechat_sdk.lib.parser import XMLStore

__author__ = 'peter'

TOKEN = "peterpetwuweixindev"
APPID = "wx29527392c853fa5b"
APPSECRET = "9b5fb899a50d2f2b8625b6e2b4a4f380"

# 百度开发者密钥
AK = "vQkltl64aGk0ppIAzul5zKeH"


# 继承wechat-python-sdk库,增加各种方法
class WeChat(WechatBasic):
    # APP信息
    __token = TOKEN
    __app_id = APPID
    __app_secret = APPSECRET

    # 登陆认证相关信息
    __auth_code = None
    __auth_access_token = None
    __refresh_token = None

    # 用户信息
    __openid = None
    __nickname = None
    __sex = None
    __province = None
    __city = None
    __country = None
    __headimgurl = None
    __privilege = None

    def __init__(self):
        super(WeChat, self).__init__(token=self.__token, appid=self.__app_id, appsecret=self.__app_secret)

    # 一系列get/set
    @property
    def auth_access_token(self):
        return self.__auth_access_token

    @property
    def openid(self):
        return self.__openid

    @property
    def nickname(self):
        return self.__nickname

    @property
    def sex(self):
        return self.__sex

    @property
    def province(self):
        return self.__province

    @property
    def city(self):
        return self.__city

    @property
    def country(self):
        return self.__country

    @property
    def headimgurl(self):
        return self.__headimgurl

    @property
    def privilege(self):
        return self.__privilege

    # 重载父类的parse_data方法,分析xml数据获取openid信息
    def parse_data(self, data):
        super(WeChat, self).parse_data(data)

        # 获得用户Open ID
        if type(data) == unicode:
            data = data.encode('utf-8')
        xml = XMLStore(xmlstring=data).xml2dict
        print(xml['FromUserName'])
        self.__openid = xml['FromUserName']

    # 设置微信公众号菜单
    def set_wechat_menu(self, menu):
        return self.create_menu(menu)

    # 通过授权code获得授权access_token和openid等信息
    def get_auth_info(self):
        if self.__auth_code is None:
            return False
        url = "https://api.weixin.qq.com/sns/oauth2/access_token"
        params = {
            'appid': self.__app_id,
            'secret': self.__app_secret,
            'code': self.__auth_code,
            'grant_type': 'authorization_code',
        }
        response = requests.get(url, params=params).json()
        # 如果包含errcode项,表示获取失败
        if 'errcode' in response:
            return False
        self.__auth_access_token = response['access_token']
        self.__refresh_token = response['refresh_token']
        self.__openid = response['openid']
        return True

    # 通过refresh_token来刷新授权access_token
    def refresh_auth_access_token(self):
        if self.__refresh_token is None:
            return False
        url = "https://api.weixin.qq.com/sns/oauth2/refresh_token"
        params = {
            'appid': self.__app_id,
            'grant_type': 'refresh_token',
            'refresh_token': self.__refresh_token,
        }
        response = requests.get(url, params=params).json()
        if 'errcode' in response:
            return False
        self.__auth_access_token = response['access_token']
        self.__refresh_token = response['refresh_token']
        self.__openid = response['openid']
        return True

    # 通过授权access_token和openid获得用户信息
    def get_auth_user_info(self):
        if self.__auth_access_token is None or self.__openid is None:
            return False
        url = "https://api.weixin.qq.com/sns/userinfo"
        params = {
            'access_token': self.__auth_access_token,
            'openid': self.__openid,
            'lang': 'zh_CN',
        }
        response = requests.get(url, params=params)
        response.encoding = 'utf-8'
        response = response.json()
        if 'errcode' in response:
            return False
        self.__openid = response['openid']
        self.__nickname = response['nickname']
        self.__sex = response['sex']
        self.__province = response['province']
        self.__city = response['city']
        self.__country = response['country']
        self.__headimgurl = response['headimgurl']
        self.__privilege = response['privilege']
        return True

    # 判断授权access_token是否有效
    def is_auth_access_token_valid(self):
        if self.__auth_access_token is None or self.__openid is None:
            return False
        url = "https://api.weixin.qq.com/sns/auth"
        params = {
            'access_token': self.__auth_access_token,
            'openid': self.__openid,
        }
        response = requests.get(url, params=params).json()
        if response['errcode'] == 0:
            return True
        return False

    # 通过code判断是否微信授权,进而获得用户信息
    # code: code信息
    # 成功返回True,失败返回False
    def auth(self, auth_code):
        if auth_code is None:
            # code为空表示用户不同意授权
            return False
        self.__auth_code = auth_code
        # 判断是否是测试用的code
        # TODO 正式发布时删除
        if auth_code == 'codeforpetwutest':
            self.set_data_for_test()
            return True

        # 获得认证信息
        if self.get_auth_info() is False:
            return False
        # 获得用户信息
        if self.get_auth_user_info() is False:
            return False
        return True

    # 填充测试用的用户数据
    def set_data_for_test(self):
        self.__openid = "oLVPpjqs9BhvzwPj5A-vTYAX3GLca"
        self.__nickname = "刺猬宝宝3"
        self.__sex = 2
        self.__city = "深圳"
        self.__province = "广东"
        self.__country = "中国"
        self.__headimgurl = "http://junzhuan.com/uc_server/images/noavatar_middle.gif"
        self.__privilege = []


# 获取WeChat的对象,应该做成全局的方式
_wechat = None


def get_wechat():
    global _wechat
    if _wechat is None:
        _wechat = WeChat()
    return _wechat


# 如果出现invalid signature的问题,很可能是https的链接被当成http了
def get_get_location_ctx(request):
    wechat = get_wechat()
    timestamp = 1414587457
    nonce_str = 'Wm3WZYTPz0wzccnW'
    url = request.build_absolute_uri()
    signature = wechat.generate_jsapi_signature(timestamp, nonce_str, url)
    return {
        'app_id': APPID,
        'timestamp': timestamp,
        'nonce_str': nonce_str,
        'signature': signature,
        'wechat': wechat,
    }
