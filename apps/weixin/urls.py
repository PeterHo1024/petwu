# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.conf.urls import url

from . import views

__author__ = 'peter'

urlpatterns = [
    url(r'^$', views.handle_request, name='handle_request'),
    url(r'^location/$', views.location, name='location'),
    url(r'^auth/$', views.auth, name='auth'),
    # TODO
    # 在正式发布时去掉
    url(r'^login/$', views.login_for_test, name='login'),
    # TODO For test
    url(r'^json_test/$', views.json_for_test, name='json'),
]
