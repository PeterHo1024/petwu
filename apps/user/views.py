# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.template.context_processors import csrf

from apps.master.models import Master
from .models import UserInfo

__author__ = 'peter'


@login_required
def user_home(request):
    return render(request, "user/home.html")


def user_reg(request):
    if request.POST:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            UserInfo.create_new(user)
            return redirect("user:login")
        else:
            return redirect("user:reg")
    form = UserCreationForm()
    ctx = {'form': form}
    ctx.update(csrf(request))
    return render(request, 'user/reg.html', ctx)


def user_logout(request):
    logout(request)
    return redirect("user:login")
