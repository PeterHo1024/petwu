# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.conf.urls import url

from . import views

__author__ = 'peter'

urlpatterns = [
    # TODO
    # 在正式发布时去掉
    url(r'^$', views.user_home, name='home'),
    url(r'^reg/$', views.user_reg, name='reg'),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'user/login.html'}, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
]
