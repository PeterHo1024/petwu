# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.forms import ModelForm
from .models import UserInfo

__author__ = 'peter'


class UserRegForm(ModelForm):
    class Meta:
        model = UserInfo
        fields = [
            'username',
            'email',
            'phone',
        ]


class UserLoginForm(ModelForm):
    class Meta:
        model = UserInfo
        fields = [
            'username',
        ]
