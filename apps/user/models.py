# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.contrib.auth.models import User

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from apps.master.models import Master

__author__ = 'peter'


@python_2_unicode_compatible
class UserInfo(models.Model):
    sitter_status_choices = (
        ("未认证", "未认证"),
        ("等待认证管理员", "等待认证管理员"),
        ("等待认证店员", "等待认证店员"),
        ("等待认证寄养师", "等待认证寄养师"),
        ("认证管理员成功", "认证管理员成功"),
        ("认证店员成功", "认证店员成功"),
        ("认证寄养师成功", "认证寄养师成功"),
    )
    # 对应认证用的User
    user = models.OneToOneField(User)
    # 绑定的微信openid
    openid = models.CharField(max_length=100, blank=True, null=True)
    # 用户名
    username = models.CharField(max_length=30)
    # 邮箱
    email = models.EmailField(blank=True)
    # 电话
    phone = models.CharField(max_length=20, blank=True)
    # 地理信息
    # longitude = models.FloatField("经度", blank=True)
    # latitude = models.FloatField("纬度", blank=True)
    # 状态
    status = models.CharField(max_length=20, blank=True)
    # 寄养者id one-to-one
    master = models.OneToOneField("master.Master")
    # 寄养师id many-to-one
    sitter = models.ForeignKey("sitter.Sitter", null=True, blank=True, on_delete=models.SET_NULL)
    # 寄养师状态
    sitter_status = models.CharField(choices=sitter_status_choices, max_length=50, blank=True, default="未认证")

    def add_new_setter(self, sitter):
        pass
        self.sitter = sitter
        if sitter.sitter_type == "寄养师":
            self.sitter_status = "等待认证寄养师"
        else:
            self.sitter_status = "等待认证管理员"
        self.save()

    @staticmethod
    def create_new(user, master=None):
        if not master:
            master = Master.objects.create(nickname=user.username)
        return UserInfo.objects.create(user=user, username=user.username, master=master)

    # 创建用户,以及其对应的UserInfo,Master
    @staticmethod
    def create_user(username, password, email=None):
        user = User.objects.create_user(username=username, password=password, email=email)
        return UserInfo.create_new(user)

    def __str__(self):
        return self.username
