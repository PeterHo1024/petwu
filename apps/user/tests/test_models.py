# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.contrib.auth import get_user_model
from django.test import TestCase

from apps.master.models import Master
from apps.user.models import UserInfo

__author__ = 'peter'

User = get_user_model()


class UserModelTest(TestCase):
    def setUp(self):
        self.username = 'username1'
        self.password = 'password1'
        self.user = User.objects.create_user(username=self.username, password=self.password)

    def test_create_user_and_user_info(self):
        user_info = UserInfo.create_user('username2', 'password1')
        self.assertEqual(user_info.username, 'username2')
        self.assertIsNotNone(user_info.master)

    def test_create_user_info_by_user(self):
        user_info = UserInfo.create_new(self.user)

        self.assertEqual(user_info.user, self.user)
        self.assertEqual(user_info.username, self.username)
        self.assertIsNotNone(user_info.master)

    def test_create_user_info_by_user_and_master(self):
        master = Master.objects.create(nickname='nickname')

        user_info = UserInfo.create_new(self.user, master=master)

        self.assertEqual(user_info.user, self.user)
        self.assertEqual(user_info.username, self.username)
        self.assertEqual(user_info.master, master)
        self.assertEqual(user_info.master.nickname, 'nickname')
