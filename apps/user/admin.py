# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.contrib import admin
from apps.img.models import Img
from .models import UserInfo

__author__ = 'peter'


class UserPictureInline(admin.StackedInline):
    model = Img
    extra = 3


class UserInfoAdmin(admin.ModelAdmin):
    search_fields = (
        'username',
    )
    inlines = [UserPictureInline]


admin.site.register(UserInfo, UserInfoAdmin)
