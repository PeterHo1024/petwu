# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('sitter', '0001_initial'),
        ('master', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('openid', models.CharField(max_length=100, null=True, blank=True)),
                ('username', models.CharField(max_length=30)),
                ('email', models.EmailField(max_length=254, blank=True)),
                ('phone', models.CharField(max_length=20, blank=True)),
                ('status', models.CharField(max_length=20, blank=True)),
                ('sitter_status', models.CharField(default='\u672a\u8ba4\u8bc1', max_length=50, blank=True, choices=[('\u672a\u8ba4\u8bc1', '\u672a\u8ba4\u8bc1'), ('\u7b49\u5f85\u8ba4\u8bc1\u7ba1\u7406\u5458', '\u7b49\u5f85\u8ba4\u8bc1\u7ba1\u7406\u5458'), ('\u7b49\u5f85\u8ba4\u8bc1\u5e97\u5458', '\u7b49\u5f85\u8ba4\u8bc1\u5e97\u5458'), ('\u7b49\u5f85\u8ba4\u8bc1\u5bc4\u517b\u5e08', '\u7b49\u5f85\u8ba4\u8bc1\u5bc4\u517b\u5e08'), ('\u8ba4\u8bc1\u7ba1\u7406\u5458\u6210\u529f', '\u8ba4\u8bc1\u7ba1\u7406\u5458\u6210\u529f'), ('\u8ba4\u8bc1\u5e97\u5458\u6210\u529f', '\u8ba4\u8bc1\u5e97\u5458\u6210\u529f'), ('\u8ba4\u8bc1\u5bc4\u517b\u5e08\u6210\u529f', '\u8ba4\u8bc1\u5bc4\u517b\u5e08\u6210\u529f')])),
                ('master', models.OneToOneField(to='master.Master')),
                ('sitter', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='sitter.Sitter', null=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
