# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.forms import ChoiceField
from django.template.loader import render_to_string

from .settings import django2html_bootstrap_settings

__author__ = 'peter'


def render_template_file(file_name, ctx):
    template_file = "django2html/bootstrap/%s.html" % file_name
    ctx['settings'] = django2html_bootstrap_settings
    return render_to_string(template_file, context=ctx)


def get_button_checkboxes(field, *args, **kwargs):
    """
    长得像按钮一样的checkbox
    :param field: 某个表单字段类
    :param args:
    :param kwargs: 其他参数
    :return:
    """
    ctx = kwargs
    # 判断field类型
    if isinstance(field, ChoiceField):
        ctx['field'] = field
        ctx['choices'] = field.choices
    return render_template_file("button_checkbox", ctx)


def get_button_radios(field, *args, **kwargs):
    """
    长得像按钮一样的radio
    :param field: 某个表单字段类
    :param args:
    :param kwargs: 其他参数
    :return:
    """
    ctx = kwargs
    ctx['field'] = field
    return render_template_file("button_radio", ctx)
