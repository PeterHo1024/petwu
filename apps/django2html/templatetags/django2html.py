# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django import template

from ..bootstrap import get_button_checkboxes, get_button_radios

__author__ = 'peter'

register = template.Library()


###############################################################################
# 表单及其控件
###############################################################################

@register.simple_tag
def button_checkboxes(field, *args, **kwargs):
    """
    长得像按钮一样的checkbox
    依赖 extra_elements.js
    :param field: 某个表单字段类
    """
    return get_button_checkboxes(field, *args, **kwargs)


@register.simple_tag
def button_radios(field, *args, **kwargs):
    """
    长得像按钮一样的radio
    依赖 extra_elements.js
    :param field: 某个表单字段类
    """
    return get_button_radios(field, *args, **kwargs)
