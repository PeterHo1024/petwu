# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

__author__ = 'peter'

django2html_bootstrap_settings = {
    'horiz_label_class': 'col-xs-3',
    'horiz_field_class': 'col-xs-9',
}
