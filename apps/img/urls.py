# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.conf.urls import url
from . import views

__author__ = 'peter'

urlpatterns = [
    url(r'^upload/(?P<img_type>[0-9a-zA-z_]+)/$', views.upload, name='upload'),
    url(r'^upload_base64/(?P<img_type>[0-9a-zA-z_]+)/$', views.upload_base64, name='upload_base64'),
    url(r'^upload_env_photo/(?P<sitter_id>[0-9]+)/$', views.upload_env_photo, name='upload_env_photo'),
    url(r'^set_desc/$', views.set_desc, name='set_desc'),
    url(r'^del/(?P<pk>[0-9]+)/$', views.remove, name='del'),
]
