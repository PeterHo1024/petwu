# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.contrib import admin
from .models import Img

__author__ = 'peter'


class ImgAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'type',
        'thumb',
        'create_date',
    )
    list_filter = (
        'type',
        'be_used',
    )
    readonly_fields = [
        'thumb',
        'create_date',
    ]


admin.site.register(Img, ImgAdmin)
