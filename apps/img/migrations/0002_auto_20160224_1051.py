# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sitter', '0001_initial'),
        ('user', '0001_initial'),
        ('img', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='img',
            name='owner_sitter',
            field=models.ForeignKey(blank=True, to='sitter.Sitter', null=True),
        ),
        migrations.AddField(
            model_name='img',
            name='user',
            field=models.ForeignKey(to='user.UserInfo'),
        ),
    ]
