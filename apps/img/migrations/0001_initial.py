# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Img',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('img', models.ImageField(upload_to='upload_imgs', verbose_name='\u56fe\u7247')),
                ('type', models.CharField(max_length=20, verbose_name='\u7c7b\u578b', choices=[('sitter_head', '\u5bc4\u517b\u5e08\u5934\u50cf'), ('master_head', '\u4e3b\u4eba\u5934\u50cf'), ('pet_head', '\u5ba0\u7269\u5934\u50cf'), ('env_photo', '\u73af\u5883\u7167\u7247')])),
                ('desc', models.TextField(max_length=100, verbose_name='\u56fe\u7247\u63cf\u8ff0', blank=True)),
                ('create_date', models.DateTimeField(auto_now_add=True, verbose_name='\u521b\u5efa\u65f6\u95f4')),
                ('be_used', models.BooleanField(default=False, verbose_name='\u662f\u5426\u4f7f\u7528')),
            ],
        ),
    ]
