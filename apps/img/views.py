# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
import base64
from django.contrib.auth.decorators import login_required
from django.core.files.base import ContentFile
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
import time
from .models import Img

__author__ = 'peter'


@csrf_exempt
@login_required
def upload(request, img_type):
    if request.FILES:
        img = Img()
        img.user = request.user.userinfo
        img.type = img_type
        img.img = request.FILES["file"]
        img.save()
        return HttpResponse(str(img.pk))

    return HttpResponse("None")


# 从POST数据中取出图片并判断大小,返回img对象
def get_upload_img(request, img_type):
    if request.POST:
        data = request.POST["file"]
        data_size = request.POST["base64Len"]
        if len(data) == int(data_size):
            if data.startswith('data:image'):
                fmt, img_str = data.split(';base64,')
                ext = fmt.split('/')[-1]
                img_data = ContentFile(base64.b64decode(img_str), name=img_type + '.' + ext)
                img = Img()
                img.user = request.user.userinfo
                img.type = img_type
                img.img = img_data
                return img
    return None


# 上传一张图片到服务器,返回图片id
@csrf_exempt
@login_required
def upload_base64(request, img_type):
    img = get_upload_img(request, img_type)
    if img:
        img.save()
        return HttpResponse(str(img.pk))
    return HttpResponse("None")


# 上传一张环境图片到服务器,将对图片设置sitter,be_used项,返回图片id
@csrf_exempt
@login_required
def upload_env_photo(request, sitter_id):
    img = get_upload_img(request, 'env_photo')
    if img:
        img.owner_sitter_id = sitter_id
        img.be_used = True
        img.save()
        return HttpResponse(str(img.pk))
    return HttpResponse("None")


# 设置图片描述
@csrf_exempt
@login_required
def set_desc(request):
    if request.POST:
        pk = request.POST.get('pk', None)
        desc = request.POST.get("desc", None)
        img = Img.objects.get(pk=int(pk))
        # 判断是否是本人在修改图片描述
        if img.owner_sitter == request.user.userinfo.sitter:
            img.desc = desc
            img.save()
            return HttpResponse("Success")
    return HttpResponse("None")


# 删除图片
@login_required
def remove(request, pk):
    img = Img.objects.get(pk=pk)
    # 判断是否是本人
    if img.owner_sitter == request.user.userinfo.sitter:
        img.delete()
        return HttpResponse("Success")
    return HttpResponse("None")
