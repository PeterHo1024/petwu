# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible

__author__ = 'peter'


@python_2_unicode_compatible
class Img(models.Model):
    type_choices = (
        ("sitter_head", "寄养师头像"),
        ("master_head", "主人头像"),
        ("pet_head", "宠物头像"),
        ("env_photo", "环境照片"),
    )
    # 上传者
    user = models.ForeignKey("user.UserInfo")
    # 图片
    img = models.ImageField("图片", upload_to='upload_imgs')
    # 图片类型
    type = models.CharField("类型", choices=type_choices, max_length=20)
    # 图片描述
    desc = models.TextField("图片描述", max_length=100, blank=True)
    # 上传时间
    create_date = models.DateTimeField("创建时间", auto_now_add=True)
    # 是否使用
    be_used = models.BooleanField("是否使用", default=False)
    # 所属寄养师(如果是寄养师的图片)
    owner_sitter = models.ForeignKey('sitter.Sitter', null=True, blank=True)

    def thumb(self):
        return '<img style="width:6rem; height:6rem" src="%s" />' % self.img.url

    thumb.allow_tags = True

    @staticmethod
    def get_sitter_env_photos(sitter):
        return Img.objects.filter(type="env_photo").filter(be_used=True).filter(owner_sitter=sitter)

    def __str__(self):
        return self.user.username + "_" + str(self.pk)


# 当Img被删除时同时删除其对应的图片
@receiver(pre_delete, sender=Img)
def on_delete(sender, instance, **kwargs):
    instance.img.delete(False)


