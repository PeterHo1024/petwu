# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import
from django.forms import ModelForm, RadioSelect, Select, TextInput, Textarea, ModelMultipleChoiceField
from django.utils.translation import ugettext_lazy as _
from apps.pet.models import Pet
from .models import Order, Img

__author__ = 'peter'


class ImgForm(ModelForm):
    class Meta:
        model = Img
        fields = (
            'pets',
            'begin_date',
            'end_date',
            'message'
        )
