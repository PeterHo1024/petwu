# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

from django.conf.urls import url

from . import views

__author__ = 'peter'


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^sitter/$', views.sitter, name='sitter'),
    url(r'^command/$', views.command, name='command'),
    url(r'^syncdb/$', views.syncdb, name='syncdb'),
    url(r'^migrate/$', views.migrate, name='migrate'),
]
