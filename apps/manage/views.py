# coding=utf-8
from __future__ import print_function, unicode_literals, absolute_import

import StringIO
import subprocess

import sys
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.management import ManagementUtility
from django.http import HttpResponse

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

__author__ = 'peter'


@login_required
@user_passes_test(lambda u: u.is_staff, redirect_field_name='user:login')
def home(request):
    ctx = {}
    return render(request, 'manage/home.html', ctx)


@login_required
@user_passes_test(lambda u: u.is_staff, redirect_field_name='user:login')
def sitter(request):
    ctx = {}
    return render(request, 'manage/sitter.html', ctx)


@csrf_exempt
def command(request):
    if request.POST:
        ret = "no echo"
        cmd = request.POST.get('cmd', None)
        if cmd:
            try:
                ret = subprocess.check_output(cmd.split(' '), stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError as e:
                ret = e.output
            except OSError as e:
                ret = e.strerror
            except Exception as e:
                ret = type(e) + str(e.args) + e.message
        return render(request, 'manage/command.html', {
            'ret': ret.replace("\n", "<br>")
        })
    else:
        return render(request, 'manage/command.html')


def syncdb(request):
    # 重定向标准输出重定向到内存的字符串缓冲(由StringIO模块提供)
    saveout = sys.stdout
    log_out = StringIO.StringIO()
    sys.stdout = log_out
    # 利用django提供的命令行工具来执行“manage.py syncdb”
    from django.core.management import execute_from_command_line
    execute_from_command_line(["manage.py", "syncdb", "--noinput"])
    # 获得“manage.py syncdb”的执行输出结果，并展示在页面
    result = log_out.getvalue()
    sys.stdout = saveout
    return HttpResponse(result.replace("\n", "<br/>"))


def migrate(request):
    # 重定向标准输出重定向到内存的字符串缓冲(由StringIO模块提供)
    saveout = sys.stdout
    log_out = StringIO.StringIO()
    sys.stdout = log_out
    # 利用django提供的命令行工具来执行“manage.py syncdb”
    from django.core.management import execute_from_command_line

    utility = ManagementUtility(["manage.py", "migrate", "--traceback"])
    cmd = utility.fetch_command("migrate")
    cmd.run_from_argv(["manage.py", "migrate", "--traceback"])

    # execute_from_command_line(["manage.py", "migrate"])
    # 获得“manage.py syncdb”的执行输出结果，并展示在页面
    result = log_out.getvalue()
    sys.stdout = saveout
    return HttpResponse(result.replace("\n", "<br/>"))
