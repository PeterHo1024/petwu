"""PetWu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from config.settings import local

admin.autodiscover()

urlpatterns = [
    url(r'^petwu/weixin/', include('apps.weixin.urls', namespace="weixin")),
    url(r'^petwu/user/', include('apps.user.urls', namespace="user")),
    url(r'^petwu/sitter/', include('apps.sitter.urls', namespace="sitter")),
    url(r'^petwu/master/', include('apps.master.urls', namespace="master")),
    url(r'^petwu/pet/', include('apps.pet.urls', namespace="pet")),
    url(r'^petwu/order/', include('apps.order.urls', namespace="order")),
    url(r'^petwu/manage/', include('apps.manage.urls', namespace="manage")),
    url(r'^petwu/img/', include('apps.img.urls', namespace="img")),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('django.contrib.auth.urls')),
]
urlpatterns += static(local.MEDIA_URL, document_root=local.MEDIA_ROOT)
